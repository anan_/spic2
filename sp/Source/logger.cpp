#include <windows.h>
#include <stdio.h>

#include "logger.h"

// ���O�o��
void Logger::Print(const char* format, ...)
{
	char message[1024];
	va_list args;
	va_start(args, format);
	vsnprintf(message, sizeof(message), format, args);
	va_end(args);

	::OutputDebugStringA(message);
}

/*
https://qiita.com/kurasho/items/1f6e04ab98d70b582ab7
*/

void Print(const char* format, ...)
{
	char message[1024];
	va_list args;
	va_start(args, format);
	vsnprintf(message, sizeof(message), format, args);
	va_end(args);

	::OutputDebugStringA(message);
}
