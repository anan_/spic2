#pragma once

#include "vector.h"
#include "wave_data.h"

class WaveManager
{
private:
	static WaveManager* instance;
	static const int Max = 10;

	WaveData* waveData;
	int count;
	int waveNum;

public:
	WaveManager() {}
	~WaveManager() {}

	void Update();

	void SetWaveData(WaveData* data) { waveData = data; }

public:
	static void Create()
	{
		if (instance != nullptr) return;
		instance = new WaveManager();
	}
	static WaveManager& GetInstance()
	{
		return *instance;
	}
	static void Destory()
	{
		if (instance != nullptr)
		{
			delete instance;
			instance = nullptr;
		}
	}
};

#define pWaveManager 

