
#include "create_DX11.h"
#include "misc.h"
#include "frame_rate.h"

#include <assert.h>
#include <vector>
#include <ctime>

#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

struct CoreSystem
{
    HWND hwnd;
    HINSTANCE hInstance;

    ComPtr<ID3D11Device> device;
    ComPtr<ID3D11DeviceContext> context;
    ComPtr<IDXGISwapChain> swap_chain;

    ComPtr<IDXGIDevice1> dxgi_device;
    ComPtr<IDXGIAdapter> dxgi_adapter;
    ComPtr<IDXGIFactory> dxgi_factory;

    ComPtr<ID3D11RenderTargetView> render_target_view;
    ComPtr<ID3D11DepthStencilView> depth_stecil_view;

    BlendState blend_state;
    RasterizerState rasterizer_state;
    DepthStencilState depth_stencil_state;

    High_Resolution_Timer high_resolution_timer;
    BenchMark benchmark;

    VECTOR4 screenColor;
};
static CoreSystem core;

namespace GetCoreSystem
{
    ID3D11Device* GetDevice()
    {
        return core.device.Get();
    }
    ID3D11DeviceContext* GetContext()
    {
        return core.context.Get();
    }
    HWND GetHWND()
    {
        return core.hwnd;
    }
    HINSTANCE GetHINSTANCE()
    {
        return core.hInstance;
    }
    BlendState* GetBlendState()
    {
        return &core.blend_state;
    }
    RasterizerState* GetRasterizerState()
    {
        return &core.rasterizer_state;
    }
    DepthStencilState* GetDepthStencilState()
    {
        return &core.depth_stencil_state;
    }
    ID3D11DepthStencilView* GetDepthStencilView()
    {
        return core.depth_stecil_view.Get();
    }
    ID3D11RenderTargetView* GetRenderTargetView()
    {
        return core.render_target_view.Get();
    }
}

namespace SetRender
{
    void SetViewPort(int width, int height)
    {
        D3D11_VIEWPORT vp;
        ZeroMemory(&vp, sizeof(vp));
        vp.Width = (FLOAT)width;
        vp.Height = (FLOAT)height;
        vp.MinDepth = 0.0f;
        vp.MaxDepth = 1.0f;
        vp.TopLeftX = 0;
        vp.TopLeftY = 0;
        core.context->RSSetViewports(1, &vp);
    }
    void SetBlender(const int blendMode)
    {
        core.blend_state.SetBlendState(blendMode);
    }
    void SetRasterizerState(const int rasterizMode)
    {
        core.rasterizer_state.SetRasterizerState(rasterizMode);
    }
    void SetDepthStencilState(const int flg)
    {
        core.depth_stencil_state.SetDepthStencilState(flg);
    }
}

// ***********************************
//            DirectX 11
// ***********************************
HRESULT Create_DX11::DeviceInit(HWND hwnd, int screenWidth, int screenHeight, bool fullScreen)
{
    using namespace GetCoreSystem;
    HRESULT hr = S_OK;

    hr = CreateDevice();
    if (FAILED(hr))
    {
        assert(!"CreateDevice");
    }
    hr = CreateSwapChain(screenWidth, screenHeight, fullScreen);
    if (FAILED(hr))
    {
        assert(!"CreateSwapChain");
    }
    hr = CreateRenderTargetView();
    if (FAILED(hr))
    {
        assert(!"CreateRenderTargetView");
    }
    hr = CreateDepthStencilView(screenWidth, screenHeight);
    if (FAILED(hr))
    {
        assert(!"CreateDepthStencilView");
    }

    hr = GetBlendState()->CreateBlendState(GetDevice() ,GetContext());

    hr = GetRasterizerState()->CreateRasterizerState(GetDevice(), GetContext());

	hr = GetDepthStencilState()->CreateDepthStencilState(GetDevice(), GetContext());

    return hr;
}

void Create_DX11::DeviceRelease()
{
    if (core.swap_chain.Get())
    {
        core.swap_chain.Get()->SetFullscreenState(false, nullptr);
    }
}

ComPtr<IDXGIFactory> global_dxgi_factor;
HRESULT Create_DX11::CreateDevice()
{
    HRESULT hr = S_OK;

    // アダプター
    ComPtr<IDXGIAdapter> dxgi_adapter;
    hr = CreateDXGIFactory(IID_PPV_ARGS(global_dxgi_factor.GetAddressOf()));
    if (FAILED(hr))
    {
        assert(!"CreateDXGIFactory");
    }
    // ファクトリー
    hr = global_dxgi_factor->EnumAdapters(0, dxgi_adapter.GetAddressOf());
    if (FAILED(hr))
    {
        assert(!"EnumAdapters");
    }

    DXGI_ADAPTER_DESC dxgi_adapter_desc;
    dxgi_adapter.Get()->GetDesc(&dxgi_adapter_desc);

    UINT create_device_flags = 0;
#if  defined(_DEBUG)
    create_device_flags |= D3D11_CREATE_DEVICE_DEBUG;
#endif
    D3D_DRIVER_TYPE driver_types[] = 
    {
        D3D_DRIVER_TYPE_UNKNOWN,
        D3D_DRIVER_TYPE_HARDWARE,
        D3D_DRIVER_TYPE_WARP,
        D3D_DRIVER_TYPE_REFERENCE,
    };
    D3D_FEATURE_LEVEL feature_levels[] =
    {
        D3D_FEATURE_LEVEL_11_1,
        D3D_FEATURE_LEVEL_11_0,
        D3D_FEATURE_LEVEL_10_1,
        D3D_FEATURE_LEVEL_10_0,
    };
    UINT num_feature_levels = ARRAYSIZE(feature_levels);

    D3D_FEATURE_LEVEL feature_level;
    for (D3D_DRIVER_TYPE driver_type : driver_types)
    {
        hr = D3D11CreateDevice(dxgi_adapter.Get(), driver_type, NULL,
            create_device_flags, feature_levels, num_feature_levels,
            D3D11_SDK_VERSION, core.device.GetAddressOf(), 
            &feature_level, core.context.GetAddressOf());
        if (SUCCEEDED(hr))
            break;
    }

    return hr;
}

HRESULT Create_DX11::CreateSwapChain(const int screenWidth, const int screenHeight, const bool fullScreen)
{
    HRESULT hr = S_OK;

    DXGI_SWAP_CHAIN_DESC swap_chain_desc;
    swap_chain_desc.BufferDesc.Width = screenWidth;
    swap_chain_desc.BufferDesc.Height = screenHeight;
    swap_chain_desc.BufferDesc.RefreshRate.Numerator = 60;
    swap_chain_desc.BufferDesc.RefreshRate.Denominator = 1;
    swap_chain_desc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB; //DXGI_FORMAT_R8G8B8A8_UNORM_SRGB DXGI_FORMAT_R8G8B8A8_UNORM
    swap_chain_desc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
    swap_chain_desc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

    swap_chain_desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    swap_chain_desc.OutputWindow = core.hwnd;
    swap_chain_desc.BufferCount = 1;
    swap_chain_desc.SampleDesc.Count = 1;
    swap_chain_desc.SampleDesc.Quality = 0;
    swap_chain_desc.Windowed = !fullScreen;
    swap_chain_desc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
    swap_chain_desc.Flags = 0;//DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

    hr = global_dxgi_factor->CreateSwapChain(core.device.Get(), &swap_chain_desc, core.swap_chain.GetAddressOf());
    if (FAILED(hr))
    {
        assert(!"CreateSwapChain");
    }
    core.swap_chain.Get()->SetFullscreenState(fullScreen, nullptr);

    return hr;
}

HRESULT Create_DX11::CreateRenderTargetView()
{
    HRESULT hr = S_OK;

    ComPtr<ID3D11Texture2D> backBuffer = nullptr;
    hr = core.swap_chain.Get()->GetBuffer(0,__uuidof(ID3D11Texture2D), (LPVOID*)&backBuffer);
    if (FAILED(hr))
    {
        assert(!"swap_chain GetBuffer");
    }

    return core.device.Get()->CreateRenderTargetView(backBuffer.Get(), nullptr, core.render_target_view.GetAddressOf());
}

HRESULT Create_DX11::CreateDepthStencilView(const int screenWidth, const int screenHeight)
{
    HRESULT hr = S_OK;

    ComPtr<ID3D11Texture2D> backBuffer = nullptr;

    D3D11_TEXTURE2D_DESC depth_stencil_desc;
    ZeroMemory(&depth_stencil_desc, sizeof(depth_stencil_desc));
    depth_stencil_desc.Width = screenWidth;
    depth_stencil_desc.Height = screenHeight;
    depth_stencil_desc.MipLevels = 1;
    depth_stencil_desc.ArraySize = 1;
    depth_stencil_desc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    depth_stencil_desc.SampleDesc.Count = 1;
    depth_stencil_desc.SampleDesc.Quality = 0;
    depth_stencil_desc.Usage = D3D11_USAGE_DEFAULT;
    depth_stencil_desc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    depth_stencil_desc.CPUAccessFlags = 0;
    depth_stencil_desc.MiscFlags = 0;
    hr = core.device.Get()->CreateTexture2D(&depth_stencil_desc, NULL, backBuffer.GetAddressOf());
    if (FAILED(hr))
    {
        assert(!"CreateTexture2D");
    }

    D3D11_DEPTH_STENCIL_VIEW_DESC depth_stencil_view_desc;
    ZeroMemory(&depth_stencil_view_desc, sizeof(depth_stencil_view_desc));
    depth_stencil_view_desc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    depth_stencil_view_desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    depth_stencil_view_desc.Texture2D.MipSlice = 0;
    hr = core.device.Get()->CreateDepthStencilView(backBuffer.Get(), &depth_stencil_view_desc, core.depth_stecil_view.GetAddressOf());

    return hr;
}


// ***********************************
//            Windows
// ***********************************
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
HWND Create_DX11::WindowsInit(const char* windowName, const int screenWidth, const int screenHeight, bool fullScreen, HINSTANCE hInstance)
{
#if defined(DEBUG) | defined(_DEBUG)
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
    //_CrtSetBreakAlloc(12572);
#endif

    // ウィンドウ生成
    WNDCLASS wcex;
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = NULL; // アイコン
    wcex.hCursor = LoadCursor(NULL, IDC_ARROW); // カーソル
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW);
    wcex.lpszMenuName = NULL;
    wcex.lpszClassName = "GameLibrary";
    RegisterClass(&wcex);

    RECT rc = { 0, 0, screenWidth, screenHeight };
    const int x = 1920 / 2 - screenWidth / 2;
    const int y = 1080 / 2 - screenHeight / 2;

    HWND hWnd;
    hWnd = CreateWindow(
        "GameLibrary", windowName,
        WS_POPUP | WS_VISIBLE | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX,
        x, y, rc.right - rc.left, rc.bottom - rc.top,
        NULL,
        NULL,
        hInstance,
        NULL);

    return hWnd;
}

void Create_DX11::WindowsRelease()
{
    UnregisterClassA("GameLibrary", core.hInstance);
}


LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{

#ifdef USE_IMGUI
    if (ImGui_ImplWin32_WndProcHandler(hWnd, message, wParam, lParam)) return 1;
#endif

    switch (message)
    {
    case WM_PAINT:
        PAINTSTRUCT ps;
        HDC hdc;
        hdc = BeginPaint(hWnd, &ps);
        EndPaint(hWnd, &ps);
        break;

    case WM_CREATE:
        break;

    case WM_CLOSE:
            PostMessage(hWnd, WM_DESTROY, 0, 0);
        break;

    case WM_DESTROY:
        PostQuitMessage(0);
        break;

    case WM_KEYDOWN:
        switch (wParam)
        {
        case VK_ESCAPE:
            PostMessage(hWnd, WM_CLOSE, 0, 0);
            return 0;
        }
        break;

    case WM_ENTERSIZEMOVE:
        // WM_EXITSIZEMOVE is sent when the user grabs the resize bars.
        core.high_resolution_timer.stop();
        break;
    case WM_EXITSIZEMOVE:
        // WM_EXITSIZEMOVE is sent when the user releases the resize bars.
        // Here we reset everything based on the new window dimensions.
        core.high_resolution_timer.start();
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }

    return 0;
}

// ImGuiのマウス座標のずれをなおす
void wndresizebyclient(HWND hWnd, int x, int y) {
    RECT size;
    RECT wndsize;
    GetClientRect(hWnd, &size);
    GetWindowRect(hWnd, &wndsize);
    wndsize.right = wndsize.right - wndsize.left;
    wndsize.bottom = wndsize.bottom - wndsize.top;
    SetWindowPos(hWnd, NULL, 0, 0, x + wndsize.right - size.right, y + wndsize.bottom - size.bottom, SWP_NOMOVE | SWP_NOREPOSITION | SWP_NOZORDER);
}

// GameLoop

bool Create_DX11::Init(const char* windowName, const int screenWidth, const int screenHeight, bool fullScreen, HINSTANCE hInstance)
{
    HRESULT hr = S_OK;

    static bool loaded = false;
    if (loaded) return loaded;

    srand((unsigned int)time(NULL));

    core.hwnd = WindowsInit(windowName, screenWidth, screenHeight, fullScreen, hInstance);
    hr = DeviceInit(core.hwnd, screenWidth, screenHeight, fullScreen);
    if (FAILED(hr))
    {
        assert(!"DeviceInit");
        return false;
    }
    core.hInstance = hInstance;
    wndresizebyclient(core.hwnd, screenWidth, screenHeight);

#ifdef USE_IMGUI
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();

    ImGui_ImplWin32_Init(core.hwnd);
    ImGui_ImplDX11_Init(core.device.Get(), core.context.Get());

    ImGui::StyleColorsDark();
#endif
    
    SetRender::SetViewPort(screenWidth, screenHeight);

    core.high_resolution_timer.reset();

    UINT m4xMsaaQuality;
    core.device.Get()->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM, 4, &m4xMsaaQuality);
    assert(m4xMsaaQuality > 0);

    loaded = true;
    return loaded;
}

void Create_DX11::Release()
{
#ifdef USE_IMGUI
    ImGui_ImplDX11_Shutdown();
    ImGui_ImplWin32_Shutdown();
    ImGui::DestroyContext();
#endif

    DeviceRelease();
    WindowsRelease();
}

bool Create_DX11::GameLoop()
{
    static int cnt = 0;
    //Back screen information to the front screen
    if(cnt > 0) 
        ScreenPresent(1, 0);
    //Presents a rendered image to the user
    ClearScreen();
    cnt++;

    MSG msg = { 0 };

    while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
    {
        if (msg.message == WM_QUIT) return false;

        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

#ifdef USE_IMGUI
    //Display frame rate for debugging only
    CalculateFrameStats(core.hwnd, &core.high_resolution_timer);

    ImGui_ImplDX11_NewFrame();
    ImGui_ImplWin32_NewFrame();
    ImGui::NewFrame();
#endif

    core.high_resolution_timer.tick();

    return true;
}

void Create_DX11::ClearScreen()
{
    using namespace GetCoreSystem;
    const float color[4] = 
    {
        core.screenColor.x,
        core.screenColor.y,
        core.screenColor.z,
        core.screenColor.w
    };

    GetContext()->ClearRenderTargetView(core.render_target_view.Get(), color);
    GetContext()->ClearDepthStencilView(core.depth_stecil_view.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
    GetContext()->OMSetRenderTargets(1, core.render_target_view.GetAddressOf(), core.depth_stecil_view.Get());
}

void Create_DX11::ScreenPresent(unsigned int sync_interval, unsigned int flags)
{
#ifdef USE_IMGUI
    ImGui::Render();
    ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
#endif

    core.swap_chain.Get()->Present(sync_interval, flags);
}

void Create_DX11::SetScreenColor(const float r, const float g, const float b)
{
    core.screenColor.x = r;
    core.screenColor.y = g;
    core.screenColor.z = b;
}

float Create_DX11::GetElapsedTime()
{
    return core.high_resolution_timer.time_interval();
}

void Create_DX11::ResetHighResolutionTimer()
{
    core.high_resolution_timer.reset();
}

float global_timer;
void Create_DX11::RenderingBegin()
{
    core.benchmark.begin();
}

void Create_DX11::RenderingEnd()
{
    global_timer = core.benchmark.end();
}

float Create_DX11::GetAverageRenderingTime()
{
    static const int MAX_SAMPLING = 100;
    static float array_timer[MAX_SAMPLING];
    ZeroMemory(&array_timer, sizeof(array_timer));
    int pointer = 0;
    array_timer[pointer] = global_timer;
    pointer = (pointer + 1) % MAX_SAMPLING;
    float sum = 1000;
    for (float& t : array_timer)
    {
        if (t == 0.0f) continue;
        if (sum > t) sum = t;
    }

    return sum;
}
