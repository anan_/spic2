
#include "mouse.h"

Mouse* Mouse::instance = nullptr;

void Mouse::Update()
{
	::GetCursorPos(&cursor);
	::GetCursorPos(&screenCursor);
	::ScreenToClient(GetHWND(), &screenCursor);

	screenPosition.x = static_cast<float>(screenCursor.x);
	screenPosition.y = static_cast<float>(screenCursor.y);

	prevPosition = position;
	position.x = static_cast<float>(cursor.x); 
	position.y = static_cast<float>(cursor.y);

	move.x = (position.x - prevPosition.x) * 0.02f;
	move.y = (position.y - prevPosition.y) * 0.02f;
}

void Mouse::Render()
{
	if (cursorImg != nullptr)
	{
		VECTOR2 size = VECTOR2(scale.x * cursorImg->GetTextureSize().x, scale.y * cursorImg->GetTextureSize().y);
		cursorImg->Render(GetContext(), position, size, VECTOR2(0, 0), cursorImg->GetTextureSize(), 1.f);
	}
}

bool Mouse::LeftButtonState(TRIGGER_MODE mode)
{
	static Key button(VK_LBUTTON);
	
	if (button.state(mode)) return true;
	if (button.state(mode)) return true;
	if (button.state(mode)) return true;

	return false;
}

bool Mouse::RightButtonState(TRIGGER_MODE mode)
{
	static Key button(VK_RBUTTON);

	if (button.state(mode)) return true;
	if (button.state(mode)) return true;
	if (button.state(mode)) return true;

	return false;
}

bool Mouse::CenterButtonState(TRIGGER_MODE mode)
{
	static Key button(VK_MBUTTON);

	if (button.state(mode)) return true;
	if (button.state(mode)) return true;
	if (button.state(mode)) return true;

	return false;
}

void Mouse::SetCursorImg(const wchar_t* filename)
{
	cursorImg = std::make_unique<Sprite>(GetDevice(), filename);
}

void Mouse::SetCursorPosition(const VECTOR2& position)
{
	int x = static_cast<int>(position.x);
	int y = static_cast<int>(position.y);
	::SetCursorPos(x, y);
}

void Mouse::SetScreenCenter(float width, float heifgt)
{
	position = VECTOR2(width / 2.f, heifgt / 2.f);
	prevPosition = VECTOR2(width / 2.f, heifgt / 2.f);
	SetCursorPosition(position);
}

