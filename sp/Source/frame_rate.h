#pragma once

#include <Windows.h>
#include "high_resolution_timer.h"

void CalculateFrameStats(HWND hwnd, High_Resolution_Timer* timer);
