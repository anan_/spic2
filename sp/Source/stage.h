#pragma once

#include "actor.h"

class Stage : public Actor
{
public:
	Stage();
	~Stage() {}

	void Move(Obj3D* obj, float elapsedTime);
};

class EraseStage : public EraseAlg
{
public:
	void Erase(Obj3D* obj, float elapsedTime)
	{
		obj->Release();
	}
};
