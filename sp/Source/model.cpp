
#include "model.h"
#include "misc.h"

Model::Model(std::shared_ptr<ModelResource>& resource)
{
	modelResource = resource;

	// ノード
	const std::vector<ModelData::Node>& resNodes = resource->GetNodes();

	nodes.resize(resNodes.size());
	for (size_t nodeIndex = 0; nodeIndex < nodes.size(); ++nodeIndex)
	{
		auto&& src = resNodes.at(nodeIndex);
		auto&& dst = nodes.at(nodeIndex);

		dst.name = src.name.c_str();
		dst.parent = src.parentIndex  >= 0 ? &nodes.at(src.parentIndex) : nullptr;
		dst.scale = src.scale;
		dst.rotate = src.rotate;
		dst.translate = src.translate;
	}
}

// アニメーション再生
void Model::PlayAnimation(int animationIndex, bool loop)
{
	currentAnimation = animationIndex;
	loopAnimation = loop;
	endAnimation = false;
	currentSeconds = 0.0f;
}

// アニメーション計算
void Model::UpdateAnimation(float elpsedTime)
{
	if (currentAnimation < 0)
	{
		return;
	}

	if (modelResource->GetAnimations().empty())
	{
		return;
	}

	const ModelData::Animation& animation = modelResource->GetAnimations().at(currentAnimation);

	const std::vector<ModelData::KeyFrame>& keyFrames = animation.keyFrames;
	int keyCount = static_cast<int>(keyFrames.size());
	for (int keyIndex = 0; keyIndex < keyCount - 1; ++keyIndex)
	{
		// 現在の時間がどのキーフレームの間にいるか判定する
		const ModelData::KeyFrame& keyFrame0 = keyFrames.at(keyIndex);
		const ModelData::KeyFrame& keyFrame1 = keyFrames.at(keyIndex + 1);
		if (currentSeconds >= keyFrame0.seconds && currentSeconds < keyFrame1.seconds)
		{
			float rate = (currentSeconds - keyFrame0.seconds / keyFrame1.seconds - keyFrame0.seconds);

			assert(nodes.size() == keyFrame0.nodeKeys.size());
			assert(nodes.size() == keyFrame1.nodeKeys.size());
			int nodeCount = static_cast<int>(nodes.size());
			for (int nodeIndex = 0; nodeIndex < nodeCount; ++nodeIndex)
			{
				// ２つのキーフレーム間の補完計算
				const ModelData::NodeKeyData& key0 = keyFrame0.nodeKeys.at(nodeIndex);
				const ModelData::NodeKeyData& key1 = keyFrame1.nodeKeys.at(nodeIndex);

				Node& node = nodes[nodeIndex];
				
				DirectX::XMVECTOR s0 = DirectX::XMLoadFloat3(&key0.scale);
				DirectX::XMVECTOR s1 = DirectX::XMLoadFloat3(&key1.scale);
				DirectX::XMVECTOR r0 = DirectX::XMLoadFloat4(&key0.rotate);
				DirectX::XMVECTOR r1 = DirectX::XMLoadFloat4(&key1.rotate);
				DirectX::XMVECTOR t0 = DirectX::XMLoadFloat3(&key0.translate);
				DirectX::XMVECTOR t1 = DirectX::XMLoadFloat3(&key1.translate);

				DirectX::XMVECTOR s = DirectX::XMVectorLerp(s0, s1, rate);
				DirectX::XMVECTOR r = DirectX::XMQuaternionSlerp(r0, r1, rate);
				DirectX::XMVECTOR t = DirectX::XMVectorLerp(t0, t1, rate);

				DirectX::XMStoreFloat3(&node.scale, s);
				DirectX::XMStoreFloat4(&node.rotate, r);
				DirectX::XMStoreFloat3(&node.translate, t);
			}
			break;
		}
	}

	// 最終フレーム処理
	if (endAnimation)
	{
		endAnimation = false;
		currentAnimation = -1;
		return;
	}

	// 時間経過
	currentSeconds += elpsedTime;
	if (currentSeconds >= animation.secondsLength)
	{
		if (loopAnimation)
		{
			currentSeconds -= animation.secondsLength;
		}
		else
		{
			currentSeconds = animation.secondsLength;
			endAnimation = true;
		}
	}
}

// ローカル変換行列計算
void Model::CalculateLocalTransform()
{
	for (Node& node : nodes)
	{
		DirectX::XMMATRIX scale, rotate, translate, local;
		scale = DirectX::XMMatrixScaling(node.scale.x, node.scale.y, node.scale.z);
		rotate = DirectX::XMMatrixRotationQuaternion(DirectX::XMVectorSet(node.rotate.x, node.rotate.y, node.rotate.z, node.rotate.w));
		translate = DirectX::XMMatrixTranslation(node.translate.x, node.translate.y, node.translate.z);

		local = scale * rotate * translate;
		DirectX::XMStoreFloat4x4(&node.localTransform, local);
	}
}

// ワールド変換行列計算
void Model::CalculateWorldTransform(const DirectX::XMMATRIX& worldMatrix)
{
	for (Node& node : nodes)
	{
		if (node.parent != nullptr)
		{
			DirectX::XMMATRIX localTransform = DirectX::XMLoadFloat4x4(&node.localTransform);
			DirectX::XMMATRIX parentWorldTransform = DirectX::XMLoadFloat4x4(&node.parent->worldTransform);
			DirectX::XMStoreFloat4x4(&node.worldTransform, localTransform * parentWorldTransform);
		}
		else
		{
			DirectX::XMMATRIX localTransform = DirectX::XMLoadFloat4x4(&node.localTransform);
			DirectX::XMStoreFloat4x4(&node.worldTransform, localTransform * worldMatrix);
		}
	}
}

int Model::RayPick(const VECTOR3& startPosition, const VECTOR3& endPosition,
	VECTOR3* outPosition, VECTOR3* outNormal, float* outLength)
{
    int ret = -1;
    DirectX::XMVECTOR start = DirectX::XMLoadFloat3(&startPosition);
    DirectX::XMVECTOR end = DirectX::XMLoadFloat3(&endPosition);
    DirectX::XMVECTOR vec = DirectX::XMVectorSubtract(end, start);
    DirectX::XMVECTOR length = DirectX::XMVector3Length(vec);
    DirectX::XMVECTOR dir = DirectX::XMVector3Normalize(vec);
    float neart;
    DirectX::XMStoreFloat(&neart, length);

    DirectX::XMVECTOR position, normal;
    for (const auto& mesh : GetModelResource()->GetMesh())
    {
        for (const auto& face: mesh.faces)
        {
            //面頂点取得
            DirectX::XMVECTOR a = DirectX::XMLoadFloat3(&face.position[0]);
            DirectX::XMVECTOR b = DirectX::XMLoadFloat3(&face.position[1]);
            DirectX::XMVECTOR c = DirectX::XMLoadFloat3(&face.position[2]);
            //3辺算出
            DirectX::XMVECTOR ab = DirectX::XMVectorSubtract(b, a);
            DirectX::XMVECTOR bc = DirectX::XMVectorSubtract(c, b);
            DirectX::XMVECTOR ca = DirectX::XMVectorSubtract(a, c);
            //外積による法線算出
            DirectX::XMVECTOR n = DirectX::XMVector3Cross(ab, bc);
            //内積の結果がプラスならば裏向き
            DirectX::XMVECTOR dot = DirectX::XMVector3Dot(dir, n);
            float fdot;
            DirectX::XMStoreFloat(&fdot, dot);
            if (fdot >= 0)continue;
            //交点算出
            DirectX::XMVECTOR cp;
            DirectX::XMVECTOR as = DirectX::XMVectorSubtract(a, start);
            DirectX::XMVECTOR d1 = DirectX::XMVector3Dot(as, n);
            DirectX::XMVECTOR x = DirectX::XMVectorDivide(d1, dot);//割り算
                                                                   //距離がnearより遠いか判定
            float fleng;
            DirectX::XMVECTOR face_leng = DirectX::XMVector3Length(x);

            DirectX::XMStoreFloat(&fleng, x);
            if (neart*neart < fleng*fleng)continue;

            cp = DirectX::XMVectorAdd(start, DirectX::XMVectorMultiply(dir, x));//足し算、かけ算
                                                                                //内点判定
            DirectX::XMVECTOR v1 = DirectX::XMVectorSubtract(a, cp);
            DirectX::XMVECTOR temp = DirectX::XMVector3Cross(v1, ab);
            DirectX::XMVECTOR work = DirectX::XMVector3Dot(temp, n);
            float fwork;
            DirectX::XMStoreFloat(&fwork, work);
            if (fwork < 0.0f)continue;
            DirectX::XMVECTOR v2 = DirectX::XMVectorSubtract(b, cp);
            temp = DirectX::XMVector3Cross(v2, bc);
            work = DirectX::XMVector3Dot(temp, n);
            DirectX::XMStoreFloat(&fwork, work);
            if (fwork < 0.0f)continue;
            DirectX::XMVECTOR v3 = DirectX::XMVectorSubtract(c, cp);
            temp = DirectX::XMVector3Cross(v3, ca);
            work = DirectX::XMVector3Dot(temp, n);
            DirectX::XMStoreFloat(&fwork, work);
            if (fwork < 0.0f)continue;

            //情報保存
            position = cp;
            normal = n;
            ret = face.materialIndex;
            neart = fleng;
        }
    }
    if (ret != -1)
    {
        DirectX::XMStoreFloat3(outPosition, position);
        DirectX::XMStoreFloat3(outNormal, normal);
    }
    *outLength = neart;
    return ret;
}
