#pragma once

#include "scene.h"
#include "scene_manager.h"

#include "sprite_data.h"

class SceneClear : public Scene
{
private:
	std::unique_ptr<SpriteData> retryImg;
	std::unique_ptr<SpriteData> returnTitleImg;

public:
    void Init(ID3D11Device* device);
    void Release();
	void Update(ID3D11Device* device, float elapsedTime);
	void Render(ID3D11DeviceContext* context, float elapsedTime);
};
