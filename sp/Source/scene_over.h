#pragma once

#include "scene.h"
#include "sprite.h"

class SceneOver : public Scene
{
public:
	std::unique_ptr<Sprite> over_ping;

    void Init(ID3D11Device* device);
    void Release();
	void Update(ID3D11Device* device, float elapsedTime);
	void Render(ID3D11DeviceContext* context, float elapsedTime);
};