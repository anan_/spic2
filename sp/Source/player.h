#pragma once

#include "actor.h"

class Player : public Actor
{
public:
	Player();
	~Player() {}
	void Move(Obj3D* obj, float elapsedTime);
};


class ErasePlayer : public EraseAlg
{
public:
	void Erase(Obj3D* obj, float elapsedTime)
	{
		obj->Release();
	}
};
