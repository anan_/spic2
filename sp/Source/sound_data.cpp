
#include "sound_data.h"

SoundData* SoundData::instance = nullptr;

LoadSound loadSound[] =
{
	// �ǉ��p     { SoundLabel::, "Data/sounds/.wav" },
    { SoundLabel::ChickenNormal, "Data/sounds/chicken_normal.wav" },
    { SoundLabel::ChickenCry, "Data/sounds/chicken-cry1.wav" },
    { SoundLabel::ChickenGrap, "Data/sounds/chicken_grap.wav" },
    { SoundLabel::BGM_1, "Data/sounds/bgm_1.wav" },

};

void SoundData::Load()
{
    for (int i = 0; i < static_cast<int>(SOUND_MAX); i++)
    {
        sound[i] = std::make_unique<Sound>(loadSound[i].filename);
    }
}

void SoundData::Play(int soundNum, bool isLoop)
{
    sound[soundNum]->Play(isLoop);
}

void SoundData::Update()
{
    for (auto& it : sound)
    {
        it->Update();
    }
}

void SoundData::Stop(int soundNum)
{
    sound[soundNum]->Stop();
}

void SoundData::SetVolume(int soundNum, const float volume)
{
	sound[soundNum]->SetVolume(volume);
}

