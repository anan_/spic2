#include	"geometric_primitive.h"

#include	<vector>
#include	"resource_manager.h"

bool GeometricPrimitive::CreateBuffers(ID3D11Device* device, Vertex* vertices, int numV, unsigned int* indices, int numI)
{
    HRESULT hr = S_OK;
  
	mesh.numVertices = numV;
	D3D11_BUFFER_DESC desc;
	{
		ZeroMemory(&desc, sizeof(desc));
		desc.Usage = D3D11_USAGE_DEFAULT;
		desc.ByteWidth = sizeof(Vertex) * mesh.numVertices;
		desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		desc.CPUAccessFlags = 0;
		desc.MiscFlags = 0;
	}
	D3D11_SUBRESOURCE_DATA data;
	{
		ZeroMemory(&data, sizeof(data));
		data.pSysMem = vertices;
	}
	hr = device->CreateBuffer(&desc, &data, mesh.vertexBuffer.GetAddressOf());
	if (FAILED(hr))
	{
		assert(!"CreateBuffer");
	}

	mesh.numIndices = numI;
	{
		ZeroMemory(&desc, sizeof(desc));
		desc.Usage = D3D11_USAGE_DEFAULT;
		desc.ByteWidth = sizeof(Vertex) * mesh.numIndices;
		desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		desc.CPUAccessFlags = 0;
		desc.MiscFlags = 0;
	}
	{
		ZeroMemory(&data, sizeof(data));
		data.pSysMem = indices;
	}
	hr = device->CreateBuffer(&desc, &data, mesh.indexBuffer.GetAddressOf());
	if (FAILED(hr))
	{
		assert(!"CreateBuffer");
	}

	cbScene = std::make_unique<ConstantBuffer<CbScene>>(device);
	cbMesh = std::make_unique<ConstantBuffer<CbMesh>>(device);
	cbSubSet = std::make_unique<ConstantBuffer<CbSubset>>(device);

    return true;
}

bool	GeometricPrimitive::CreateBuffers(ID3D11Device* device,
	const std::vector<Vertex>& vertices,
	const std::vector<unsigned int>& indices)
{
	HRESULT hr = S_OK;

	mesh.numVertices = static_cast<int>(vertices.size());
	D3D11_BUFFER_DESC desc;
	{
		ZeroMemory(&desc, sizeof(desc));
		desc.Usage = D3D11_USAGE_DEFAULT;
		desc.ByteWidth = sizeof(Vertex) * mesh.numVertices;
		desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		desc.CPUAccessFlags = 0;
		desc.MiscFlags = 0;
	}
	D3D11_SUBRESOURCE_DATA data;
	{
		ZeroMemory(&data, sizeof(data));
		data.pSysMem = vertices.data();
	}
	hr = device->CreateBuffer(&desc, &data, mesh.vertexBuffer.GetAddressOf());
	if (FAILED(hr))
	{
		assert(!"CreateBuffer");
	}

	mesh.numIndices = static_cast<int>(indices.size());
	{
		ZeroMemory(&desc, sizeof(desc));
		desc.Usage = D3D11_USAGE_DEFAULT;
		desc.ByteWidth = sizeof(Vertex) * mesh.numIndices;
		desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		desc.CPUAccessFlags = 0;
		desc.MiscFlags = 0;
	}
	{
		ZeroMemory(&data, sizeof(data));
		data.pSysMem = indices.data();
	}
	hr = device->CreateBuffer(&desc, &data, mesh.indexBuffer.GetAddressOf());
	if (FAILED(hr))
	{
		assert(!"CreateBuffer");
	}

	cbScene = std::make_unique<ConstantBuffer<CbScene>>(device);
	cbMesh = std::make_unique<ConstantBuffer<CbMesh>>(device);
	cbSubSet = std::make_unique<ConstantBuffer<CbSubset>>(device);

	return true;
}

void GeometricPrimitive::Update()
{
	DirectX::XMMATRIX S, R, T, W;
	S = DirectX::XMMatrixScaling(scale.x, scale.y, scale.z);
	R = DirectX::XMMatrixRotationRollPitchYaw(angle.x, angle.y, angle.z);
	T = DirectX::XMMatrixTranslation(position.x, position.y, position.z);
	W = S * R * T;
	DirectX::XMStoreFloat4x4(&world, W);
}

void GeometricPrimitive::Render(ID3D11DeviceContext* context, const FLOAT4X4& viewProjection, const VECTOR4& color)
{
	shader->Activate(context);

	cbScene->data.view_projection = viewProjection;
	cbMesh->data.world = world;
	cbSubSet->data.materialColor = color;
	cbScene->Activate(context, 0);
	cbMesh->Activate(context, 1);
	cbSubSet->Activate(context, 2);

	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	UINT stride = sizeof(Vertex);
	UINT offset = 0;
	context->IASetVertexBuffers(0, 1, mesh.vertexBuffer.GetAddressOf(), &stride, &offset);
	context->IASetIndexBuffer(mesh.indexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);

	context->DrawIndexed(mesh.numIndices, 0, 0);
}

GeometricCube::GeometricCube(ID3D11Device* device) : GeometricPrimitive(device)
{
	//std::vector<Vertex> vertices;
	//std::vector<unsigned int> indices;
	vertices.resize(4 * 6);
	indices.resize(3 * 2 * 6);

	int numV = 0, numI = 0;

	//	上面
	vertices[numV + 0].position = VECTOR3(-0.5f, +0.5f, +0.5f);
	vertices[numV + 1].position = VECTOR3(+0.5f, +0.5f, +0.5f);
	vertices[numV + 2].position = VECTOR3(-0.5f, +0.5f, -0.5f);
	vertices[numV + 3].position = VECTOR3(+0.5f, +0.5f, -0.5f);
	vertices[numV + 0].normal = vertices[numV + 1].normal =
		vertices[numV + 2].normal =
		vertices[numV + 3].normal = VECTOR3(+0.0f, +1.0f, +0.0f);
	indices[numI + 0] = numV + 0;	indices[numI + 1] = numV + 1;	indices[numI + 2] = numV + 2;
	indices[numI + 3] = numV + 1;	indices[numI + 4] = numV + 3;	indices[numI + 5] = numV + 2;
	numV += 4;	numI += 6;

	//	下面
	vertices[numV + 0].position = VECTOR3(-0.5f, -0.5f, +0.5f);
	vertices[numV + 1].position = VECTOR3(+0.5f, -0.5f, +0.5f);
	vertices[numV + 2].position = VECTOR3(-0.5f, -0.5f, -0.5f);
	vertices[numV + 3].position = VECTOR3(+0.5f, -0.5f, -0.5f);
	vertices[numV + 0].normal = vertices[numV + 1].normal =
		vertices[numV + 2].normal =
		vertices[numV + 3].normal = VECTOR3(+0.0f, -1.0f, +0.0f);
	indices[numI + 0] = numV + 0;	indices[numI + 1] = numV + 2;	indices[numI + 2] = numV + 1;
	indices[numI + 3] = numV + 1;	indices[numI + 4] = numV + 2;	indices[numI + 5] = numV + 3;
	numV += 4;	numI += 6;

	//	右面
	vertices[numV + 0].position = VECTOR3(+0.5f, +0.5f, -0.5f);
	vertices[numV + 1].position = VECTOR3(+0.5f, +0.5f, +0.5f);
	vertices[numV + 2].position = VECTOR3(+0.5f, -0.5f, -0.5f);
	vertices[numV + 3].position = VECTOR3(+0.5f, -0.5f, +0.5f);
	vertices[numV + 0].normal = vertices[numV + 1].normal =
		vertices[numV + 2].normal =
		vertices[numV + 3].normal = VECTOR3(+1.0f, +0.0f, +0.0f);
	indices[numI + 0] = numV + 0;	indices[numI + 1] = numV + 1;	indices[numI + 2] = numV + 2;
	indices[numI + 3] = numV + 1;	indices[numI + 4] = numV + 3;	indices[numI + 5] = numV + 2;
	numV += 4;	numI += 6;

	//	左面
	vertices[numV + 0].position = VECTOR3(-0.5f, +0.5f, -0.5f);
	vertices[numV + 1].position = VECTOR3(-0.5f, +0.5f, +0.5f);
	vertices[numV + 2].position = VECTOR3(-0.5f, -0.5f, -0.5f);
	vertices[numV + 3].position = VECTOR3(-0.5f, -0.5f, +0.5f);
	vertices[numV + 0].normal = vertices[numV + 1].normal =
		vertices[numV + 2].normal =
		vertices[numV + 3].normal = VECTOR3(-1.0f, +0.0f, +0.0f);
	indices[numI + 0] = numV + 0;	indices[numI + 1] = numV + 2;	indices[numI + 2] = numV + 1;
	indices[numI + 3] = numV + 1;	indices[numI + 4] = numV + 2;	indices[numI + 5] = numV + 3;
	numV += 4;	numI += 6;

	//	後面
	vertices[numV + 0].position = VECTOR3(+0.5f, -0.5f, +0.5f);
	vertices[numV + 1].position = VECTOR3(+0.5f, +0.5f, +0.5f);
	vertices[numV + 2].position = VECTOR3(-0.5f, -0.5f, +0.5f);
	vertices[numV + 3].position = VECTOR3(-0.5f, +0.5f, +0.5f);
	vertices[numV + 0].normal = vertices[numV + 1].normal =
		vertices[numV + 2].normal =
		vertices[numV + 3].normal = VECTOR3(+0.0f, +0.0f, +1.0f);
	indices[numI + 0] = numV + 0;	indices[numI + 1] = numV + 1;	indices[numI + 2] = numV + 2;
	indices[numI + 3] = numV + 1;	indices[numI + 4] = numV + 3;	indices[numI + 5] = numV + 2;
	numV += 4;	numI += 6;

	//	前面
	vertices[numV + 0].position = VECTOR3(+0.5f, -0.5f, -0.5f);
	vertices[numV + 1].position = VECTOR3(+0.5f, +0.5f, -0.5f);
	vertices[numV + 2].position = VECTOR3(-0.5f, -0.5f, -0.5f);
	vertices[numV + 3].position = VECTOR3(-0.5f, +0.5f, -0.5f);
	vertices[numV + 0].normal = vertices[numV + 1].normal =
		vertices[numV + 2].normal =
		vertices[numV + 3].normal = VECTOR3(+0.0f, +0.0f, -1.0f);
	indices[numI + 0] = numV + 0;	indices[numI + 1] = numV + 2;	indices[numI + 2] = numV + 1;
	indices[numI + 3] = numV + 1;	indices[numI + 4] = numV + 2;	indices[numI + 5] = numV + 3;
	numV += 4;	numI += 6;

	CreateBuffers(device, vertices, indices);
}

GeometricRect::GeometricRect(ID3D11Device* device) : GeometricPrimitive(device)
{
	//vertices.resize(4);
	//indices.resize(3 * 2);

	//CreateBuffers(device, vertices, indices);
}

GeometricBoard::GeometricBoard(ID3D11Device* device) : GeometricPrimitive(device)
{
	//vertices.resize(4);
	//indices.resize(3 * 2);

	//CreateBuffers(device, vertices, indices);
}

GeometricSphere::GeometricSphere(ID3D11Device* device, int slices, int stacks) : GeometricPrimitive(device)
{
	//std::vector<Vertex> vertices;
	//std::vector<u_int> indices;


	float r = 0.5f;		//	半径 0.5f = 直径 1.0f

	//
	// Compute the vertices stating at the top pole and moving down the stacks.
	//

	// Poles: note that there will be texture coordinate distortion as there is
	// not a unique point on the texture map to assign to the pole when mapping
	// a rectangular texture onto a sphere.
	Vertex top_vertex;
	top_vertex.position = VECTOR3(0.0f, +r, 0.0f);
	top_vertex.normal = VECTOR3(0.0f, +1.0f, 0.0f);

	Vertex bottom_vertex;
	bottom_vertex.position = VECTOR3(0.0f, -r, 0.0f);
	bottom_vertex.normal = VECTOR3(0.0f, -1.0f, 0.0f);

	vertices.push_back(top_vertex);

	float phi_step = DirectX::XM_PI / stacks;
	float theta_step = 2.0f * DirectX::XM_PI / slices;

	// Compute vertices for each stack ring (do not count the poles as rings).
	for (u_int i = 1; i <= stacks - 1; ++i)
	{
		float phi = i * phi_step;
		float rs_phi = r * sinf(phi), rc_phi = r * cosf(phi);

		// Vertices of ring.
		for (u_int j = 0; j <= slices; ++j)
		{
			float theta = j * theta_step;

			Vertex v;

			// spherical to cartesian
			v.position.x = rs_phi * cosf(theta);
			v.position.y = rc_phi;
			v.position.z = rs_phi * sinf(theta);

			DirectX::XMVECTOR p = DirectX::XMLoadFloat3(&v.position);
			DirectX::XMStoreFloat3(&v.normal, DirectX::XMVector3Normalize(p));

			vertices.push_back(v);
		}
	}

	vertices.push_back(bottom_vertex);

	//
	// Compute indices for top stack.  The top stack was written first to the vertex buffer
	// and connects the top pole to the first ring.
	//
	for (UINT i = 1; i <= slices; ++i)
	{
		indices.push_back(0);
		indices.push_back(i + 1);
		indices.push_back(i);
	}

	//
	// Compute indices for inner stacks (not connected to poles).
	//

	// Offset the indices to the index of the first vertex in the first ring.
	// This is just skipping the top pole vertex.
	u_int base_index = 1;
	u_int ring_vertex_count = slices + 1;
	for (u_int i = 0; i < stacks - 2; ++i)
	{
		u_int i_rvc = i * ring_vertex_count;
		u_int i1_rvc = (i + 1) * ring_vertex_count;

		for (u_int j = 0; j < slices; ++j)
		{
			indices.push_back(base_index + i_rvc + j);
			indices.push_back(base_index + i_rvc + j + 1);
			indices.push_back(base_index + i1_rvc + j);

			indices.push_back(base_index + i1_rvc + j);
			indices.push_back(base_index + i_rvc + j + 1);
			indices.push_back(base_index + i1_rvc + j + 1);
		}
	}

	//
	// Compute indices for bottom stack.  The bottom stack was written last to the vertex buffer
	// and connects the bottom pole to the bottom ring.
	//

	// South pole vertex was added last.
	u_int south_pole_index = (u_int)vertices.size() - 1;

	// Offset the indices to the index of the first vertex in the last ring.
	base_index = south_pole_index - ring_vertex_count;

	for (u_int i = 0; i < slices; ++i)
	{
		indices.push_back(south_pole_index);
		indices.push_back(base_index + i);
		indices.push_back(base_index + i + 1);
	}

	CreateBuffers(device, vertices.data(), vertices.size(), indices.data(), indices.size());
}

//GeometricSphere2::GeometricSphere2(ID3D11Device* device, u_int div)
//{
//	//vertices.resize(4);
//	//indices.resize(3 * 2);
//
//	//CreateBuffers(device, vertices, indices);
//}
