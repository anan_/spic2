//
//#include "board.h"
//#include "create_DX11.h"
//#include "resource_manager.h"
//
//
//Board::Board(ID3D11Device* device, const wchar_t* filename, bool oblon)
//{
//	HRESULT hr = S_OK;
//	if (!oblon)
//	{
//		lt = VECTOR3(-0.5f, +0.5f, 0.0f);
//		rt = VECTOR3(+0.5f, +0.5f, 0.0f);
//		lb = VECTOR3(-0.5f, -0.5f, 0.0f);
//		rb= VECTOR3(+0.5f, -0.5f, 0.0f);
//	}
//	else
//	{
//		lt = VECTOR3(-1.0f, +0.5f, 0.0f);
//		rt = VECTOR3(+1.0f, +0.5f, 0.0f);
//		lb = VECTOR3(-1.0f, -0.5f, 0.0f);
//		rb = VECTOR3(+1.0f, -0.5f, 0.0f);
//	}
//
//	Vertex vertices[4] =
//	{
//		{ lt, VECTOR2(1, 0) },
//		{ rt, VECTOR2(0, 0) },
//		{ lb, VECTOR2(1, 1) },
//		{ rb, VECTOR2(0, 1) },
//	};
//
//	{
//		D3D11_BUFFER_DESC desc;
//		ZeroMemory(&desc, sizeof(desc));
//		desc.Usage = D3D11_USAGE_DYNAMIC;
//		desc.ByteWidth = sizeof(Vertex) * 4;
//		desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
//		desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
//		D3D11_SUBRESOURCE_DATA data;
//		ZeroMemory(&data, sizeof(data));
//		data.pSysMem = vertices;
//		hr = device->CreateBuffer(&desc, &data, buffer.GetAddressOf());
//		if (FAILED(hr))
//		{
//			assert(!"VertexBuffer Board");
//		}
//	}
//	// 定数バッファを作成するための設定オプション
//	D3D11_BUFFER_DESC buffer_desc = {};
//	buffer_desc.ByteWidth = sizeof(cbuffer);	// バッファ（データを格納する入れ物）のサイズ
//	buffer_desc.Usage = D3D11_USAGE_DEFAULT;
//	buffer_desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;	// 定数バッファとしてバッファを作成する。
//	buffer_desc.CPUAccessFlags = 0;
//	buffer_desc.MiscFlags = 0;
//	buffer_desc.StructureByteStride = 0;
//	hr = device->CreateBuffer(&buffer_desc, nullptr, constantBuffer.GetAddressOf());
//	if (FAILED(hr))
//	{
//		assert(0);
//	}
//	shader = std::make_unique<Shader>();
//	shader->CreateBoard(device);
//
//	
//
//	{
//		D3D11_RASTERIZER_DESC desc;
//		ZeroMemory(&desc, sizeof(desc));
//		desc.FillMode = D3D11_FILL_SOLID;
//		desc.CullMode = D3D11_CULL_NONE;
//		desc.FrontCounterClockwise = TRUE;
//		desc.DepthBias = 0;
//		desc.DepthBiasClamp = 0;
//		desc.SlopeScaledDepthBias = 0;
//		desc.DepthClipEnable = TRUE;
//		desc.ScissorEnable = TRUE;
//		desc.MultisampleEnable = TRUE;
//		desc.AntialiasedLineEnable = FALSE;
//		hr = device->CreateRasterizerState(&desc, rasterizer.GetAddressOf());
//		if (FAILED(hr))
//		{
//			assert(!"RasterizerState Board");
//		}
//	}
//
//	{
//		D3D11_SAMPLER_DESC desc;
//		ZeroMemory(&desc, sizeof(desc));
//		desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
//		desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
//		desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
//		desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
//		desc.ComparisonFunc = D3D11_COMPARISON_NEVER;
//		desc.MinLOD = -FLT_MAX;
//		desc.MaxLOD = FLT_MAX;
//		desc.MaxAnisotropy = 16;
//		memcpy(desc.BorderColor, &VECTOR4(1.0f, 10.f, 10.f, 10.f), sizeof(DirectX::XMFLOAT4));
//		hr = device->CreateSamplerState(&desc, sampler.GetAddressOf());
//		if (FAILED(hr))
//		{
//			assert(!"SamplerState Board");
//		}
//	}
//
//	{
//		D3D11_DEPTH_STENCIL_DESC desc;
//		ZeroMemory(&desc, sizeof(desc));
//		desc.DepthEnable = TRUE;
//		desc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
//		desc.DepthFunc = D3D11_COMPARISON_LESS;
//		desc.StencilEnable = FALSE;
//		desc.StencilReadMask = 0xFF;
//		desc.StencilWriteMask = 0xFF;
//		desc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
//		desc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
//		desc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
//		desc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
//		desc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
//		desc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
//		desc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
//		desc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
//		hr = device->CreateDepthStencilState(&desc, depthStensilState.GetAddressOf());
//		if (FAILED(hr))
//		{
//			assert(!"DepthStencilState Board");
//		}
//	}
//
//	hr = ResourceManager::LoadShaderResourceView(device, filename, srv.GetAddressOf(), &texture2dDesc);
//	if (FAILED(hr)) {
//		assert(0);
//	}
//
//}
//
//void Board::Render(ID3D11DeviceContext* context, const VECTOR3& position, const float& scale,
//	const FLOAT4X4& view, const FLOAT4X4& projection, const VECTOR4& color)
//{
//	UINT stride = sizeof(Vertex);
//	UINT offset = 0;
//
//	context->IASetVertexBuffers(0, 1, buffer.GetAddressOf(), &stride, &offset);
//	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
//
//	shader->Activate(context);
//
//	context->RSSetState(rasterizer.Get());
//
//	context->PSSetShaderResources(0, 1, srv.GetAddressOf());
//	context->PSSetSamplers(0, 1, sampler.GetAddressOf());
//
//	context->OMSetDepthStencilState(depthStensilState.Get(), 0);
//
//	{
//		DirectX::XMMATRIX S, T, W, WVP;
//		{
//			S = DirectX::XMMatrixScaling(scale, scale, scale);
//			T = DirectX::XMMatrixTranslation(position.x, position.y, position.z);
//			W = S * T;
//		}
//		DirectX::XMMATRIX V, P, invView;
//		{
//			V = DirectX::XMLoadFloat4x4(&view);
//			P = DirectX::XMLoadFloat4x4(&projection);
//			FLOAT4X4 temp = view;
//			// 位置情報削除
//			temp._41 = 0.0f; temp._42 = 0.0f;
//			temp._43 = 0.0f; temp._44 = 1.0f;
//			invView = DirectX::XMLoadFloat4x4(&temp);
//			invView = DirectX::XMMatrixInverse(nullptr, invView);
//			WVP = invView * W * V * P;
//			FLOAT4X4 wvp;
//			DirectX::XMStoreFloat4x4(&wvp, WVP);
//
//			cbuffer cb;
//			cb.worldViewProjection = wvp;
//			cb.color = color;
//			context->UpdateSubresource(constantBuffer.Get(), 0, nullptr, &cb, 0, 0);
//			context->VSSetConstantBuffers(0, 1, constantBuffer.GetAddressOf());
//		}
//	}
//
//	context->Draw(4, 0);
//}
//
#include "Board.h"
#include "resource_manager.h"
Board::Board(ID3D11Device* device, const wchar_t* file_name)
{
	HRESULT hr;
	vertex vertices[] =
	{
		{ VECTOR3(-0.5f, +0.5f, 0.0f), VECTOR2(1, 0),VECTOR4(1.f, 1.f, 1.f,1.f) },
		{ VECTOR3(+0.5f, +0.5f, 0.0f), VECTOR2(0, 0),VECTOR4(1.f, 1.f, 1.f,1.f) },
		{ VECTOR3(-0.5f, -0.5f, 0.0f), VECTOR2(1, 1),VECTOR4(1.f, 1.f, 1.f,1.f) },
		{ VECTOR3(+0.5f, -0.5f, 0.0f), VECTOR2(0, 1),VECTOR4(1.f, 1.f, 1.f,1.f) },
	};
	D3D11_BUFFER_DESC structure = {};
	ZeroMemory(&structure, sizeof(structure));
	structure.Usage = D3D11_USAGE_DYNAMIC;
	structure.ByteWidth = sizeof(vertex) * 4;
	structure.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	structure.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	D3D11_SUBRESOURCE_DATA structure_data;
	ZeroMemory(&structure_data, sizeof(structure_data));
	structure_data.pSysMem = vertices;
	hr = device->CreateBuffer(&structure, &structure_data, &buffer);
	if (FAILED(hr))
	{
		assert(0);
	}
	// 定数バッファを作成するための設定オプション
	D3D11_BUFFER_DESC buffer_desc = {};
	buffer_desc.ByteWidth = sizeof(cbuffer);	// バッファ（データを格納する入れ物）のサイズ
	buffer_desc.Usage = D3D11_USAGE_DEFAULT;
	buffer_desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;	// 定数バッファとしてバッファを作成する。
	buffer_desc.CPUAccessFlags = 0;
	buffer_desc.MiscFlags = 0;
	buffer_desc.StructureByteStride = 0;
	hr = device->CreateBuffer(&buffer_desc, nullptr, &constant_buffer);
	if (FAILED(hr))
	{
		assert(0);
	}
	shader = std::make_unique<Shader>();
	shader->CreateBoard(device);
	//	shader->CreateBoard(device);
	//シェーダー
	D3D11_INPUT_ELEMENT_DESC input_element[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT numElements = ARRAYSIZE(input_element);
	const char* vsName = "Data/shaders/cso/sprite_vs.cso";
	const char* psName = "Data/shaders/cso/sprite_ps.cso";

	hr = ResourceManager::LoadVertexShaders(device, vsName, input_element, numElements, &vertexShader, &input);
	if (FAILED(hr))
	{
		assert(0);
	}
	hr = ResourceManager::LoadPixelShaders(device, psName, &pixelShader);
	if (FAILED(hr))
	{
		assert(0);
	}

	//
	D3D11_RASTERIZER_DESC noCullDesc;
	ZeroMemory(&noCullDesc, sizeof(D3D11_RASTERIZER_DESC));
	noCullDesc.FillMode = D3D11_FILL_SOLID;
	noCullDesc.CullMode = D3D11_CULL_NONE;
	noCullDesc.FrontCounterClockwise = true;
	noCullDesc.DepthBias = 0;
	noCullDesc.DepthBiasClamp = 0.0f;
	noCullDesc.SlopeScaledDepthBias = 0.0f;
	noCullDesc.DepthClipEnable = true;
	noCullDesc.ScissorEnable = false;
	noCullDesc.MultisampleEnable = true;
	noCullDesc.AntialiasedLineEnable = false;
	hr = device->CreateRasterizerState(&noCullDesc, &rasterrizer);
	if (FAILED(hr))
	{
		assert(0);
	}
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = -FLT_MAX;
	sampDesc.MaxLOD = FLT_MAX;
	sampDesc.MaxAnisotropy = 16;
	memcpy(sampDesc.BorderColor, &VECTOR4(1.0f, 10.f, 10.f, 10.f), sizeof(VECTOR4));
	hr = device->CreateSamplerState(&sampDesc, &samplerstate);
	if (FAILED(hr)) {
		assert(0);
	}
	//UNIT06
	D3D11_DEPTH_STENCIL_DESC depthsteDesc;
	ZeroMemory(&depthsteDesc, sizeof(depthsteDesc));
	depthsteDesc.DepthEnable = TRUE;
	depthsteDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
	depthsteDesc.DepthFunc = D3D11_COMPARISON_LESS;
	depthsteDesc.StencilEnable = FALSE;
	depthsteDesc.StencilReadMask = 0xFF;
	depthsteDesc.StencilWriteMask = 0xFF;
	depthsteDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthsteDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthsteDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthsteDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depthsteDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthsteDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthsteDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthsteDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	hr = device->CreateDepthStencilState(&depthsteDesc, &depthstencil);
	if (FAILED(hr)) {
		assert(0);
	}
	//if (file_name == L"")
	//{
	//	//ダミーテクスチャ作成
	//	static unsigned char image[]{ 255,255,255 };
	//	D3D11_TEXTURE2D_DESC desc;
	//	D3D11_SUBRESOURCE_DATA initialData;
	//	ID3D11Texture2D* texture = NULL;

	//	//ZeroMemory(&desc, sizeof(D3D11_TEXTURE2D_DESC));
	//	desc.Width = 1;
	//	desc.Height = 1;
	//	desc.MipLevels = 1;
	//	desc.ArraySize = 1;
	//	desc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	//	desc.SampleDesc.Count = 1;
	//	desc.SampleDesc.Quality = 0;
	//	desc.Usage = D3D11_USAGE_DEFAULT;
	//	desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	//	desc.CPUAccessFlags = 0;
	//	desc.MiscFlags = 0;

	//	initialData.pSysMem = image;
	//	initialData.SysMemPitch = 1;
	//	initialData.SysMemSlicePitch = 0;

	//	hr = device->CreateTexture2D(&desc, &initialData, &texture);
	//	if (FAILED(hr)) {
	//		assert(0);
	//	}
	//	ID3D11ShaderResourceView* textureRview = NULL;
	//	//ID3DX11EffectShaderResourceVariable* textureRVar = NULL;

	//	D3D11_SHADER_RESOURCE_VIEW_DESC SRVDesc;
	//	ZeroMemory(&SRVDesc, sizeof(SRVDesc));
	//	SRVDesc.Format = desc.Format;
	//	SRVDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	//	SRVDesc.Texture2D.MostDetailedMip = 0;
	//	SRVDesc.Texture2D.MipLevels = 1;

	//	ID3D11Resource*resource = nullptr;
	//	const D3D11_SHADER_RESOURCE_VIEW_DESC shader_resoirce_view_desc;
	//	hr = device->CreateShaderResourceView(texture, &SRVDesc, &srview);
	//	if (FAILED(hr)) {
	//		assert(0);
	//	}
	//}
	//else
	//{
	hr = ResourceManager::LoadShaderResourceView(device, file_name, &srview, &texture2d);
	if (FAILED(hr)) {
		assert(0);
	}


}

//Board::~Board()
//{
//	if (vertexshader != nullptr)
//	{
//		vertexshader->Release();
//		vertexshader = nullptr;
//	}
//	if (pixe != nullptr)
//	{
//		pixe->Release();
//		pixe = nullptr;
//	}
//	if (buffer != nullptr)
//	{
//		buffer->Release();
//		buffer = nullptr;
//	}
//	if (rasterrizer != nullptr)
//	{
//		rasterrizer->Release();
//		rasterrizer = nullptr;
//	}
//	if (srview != nullptr)
//	{
//		srview->Release();
//		srview = nullptr;
//	}
//	if (samplerstate != nullptr)
//	{
//		samplerstate->Release();
//		samplerstate = nullptr;
//	}
//	if (depthstencil != nullptr)
//	{
//		depthstencil->Release();
//		depthstencil = nullptr;
//	}
//	if (input != nullptr)
//	{
//		input->Release();
//		input = nullptr;
//	}
//	if (constant_buffer != nullptr)
//	{
//		constant_buffer->Release();
//		constant_buffer = nullptr;
//	}
//
//}

void Board::render(ID3D11DeviceContext* immediate_context, const VECTOR3& position, const float& scale, 
	const FLOAT4X4& view, const FLOAT4X4& projection, const VECTOR4& color)
{
	UINT stride = sizeof(vertex);
	UINT offset = 0;

	immediate_context->IASetVertexBuffers(0, 1, &buffer, &stride, &offset);

	immediate_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	immediate_context->IASetInputLayout(input);

	immediate_context->RSSetState(rasterrizer);

	immediate_context->VSSetShader(vertexShader, NULL, 0);

	immediate_context->PSSetShader(pixelShader, NULL, 0);
	immediate_context->PSSetShaderResources(0, 1, &srview);

	immediate_context->PSSetSamplers(0, 1, &samplerstate);
	immediate_context->OMSetDepthStencilState(depthstencil, 0);

	{
		//scaleとpositionからワールド行列を作成
		DirectX::XMMATRIX S, R, T, W, WVP;
		{
			S = DirectX::XMMatrixScaling(scale, scale, scale);
			//R = DirectX::XMMatrixRotationRollPitchYaw(angle.x, angle.y, angle.z);
			T = DirectX::XMMatrixTranslation(position.x, position.y, position.z);
			W = S  * T;
		}
		//	Matrix型へ変換
		DirectX::XMMATRIX V, P;
		V = DirectX::XMLoadFloat4x4(&view);
		P = DirectX::XMLoadFloat4x4(&projection);
		DirectX::XMFLOAT4X4	view2 = view;
		DirectX::XMMATRIX		inv_view2;
		//	位置情報だけを削除
		view2._41 = 0.0f; view2._42 = 0.0f;
		view2._43 = 0.0f; view2._44 = 1.0f;
		inv_view2 = DirectX::XMLoadFloat4x4(&view2);
		//	view行列の逆行列作成
		inv_view2 = DirectX::XMMatrixInverse(nullptr, inv_view2);
		WVP = inv_view2 * W * V * P;
		DirectX::XMFLOAT4X4 wvp;
		DirectX::XMStoreFloat4x4(&wvp, WVP);
		//
		cbuffer cb;
		cb.world_view_projection = wvp;
		cb.material_color = color;
		immediate_context->UpdateSubresource(constant_buffer, 0, nullptr, &cb, 0, 0);
		immediate_context->VSSetConstantBuffers(0, 1, &constant_buffer);

	}

	immediate_context->Draw(4, 0);

}


