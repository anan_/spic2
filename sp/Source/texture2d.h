//#pragma once
//
//#include "sprite.h"
//
//enum Tex2DNo
//{
//	Title,
//	Game,
//	Result,
//
//	Number,
//
//	Max
//};
//const UINT Texture2DMax = static_cast<UINT>(Tex2DNo::Max);
//
//struct LoadTexture
//{
//	int texNum;
//	const wchar_t* filename;
//	UINT maxInstance;
//};
//
//class Texture2D
//{
//	static Texture2D* instance;
//
//private:
//	std::unique_ptr<SpriteBatch> texture2d[Texture2DMax];
//
//public:
//	Texture2D() {}
//	~Texture2D() {}
//
//	void Load(ID3D11Device* device);
//
//	void DrawCenter(ID3D11DeviceContext* context, int texNo,
//		const VECTOR2& position, const VECTOR2& size,
//		const VECTOR2& texPos, const VECTOR2& texSize,
//		const VECTOR2& center, float angle,
//		const VECTOR4& color);
//
//	void DrawLeft(ID3D11DeviceContext* context, int texNo,
//		const VECTOR2& position, const VECTOR2& size,
//		const VECTOR2& texPos, const VECTOR2& texSize,
//		const VECTOR2& center, float angle,
//		const VECTOR4& color);
//
//public:
//	static void Create()
//	{
//		if (instance != nullptr) return;
//		instance = new Texture2D;
//	}
//	static Texture2D& GetInstance()
//	{
//		return *instance;
//	}
//	static void Destory()
//	{
//		if (instance != nullptr)
//		{
//			delete instance;
//			instance = nullptr;
//		}
//	}
//};
//
//#define pTexture2D Texture2D::GetInstance()
// 