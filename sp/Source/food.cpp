
#include "food.h"
#include "load_model.h"
#include "input.h"
#include "random.h"
#include "player.h"

#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

void Food::Move(Obj3D* obj, float elapsedTime)
{
	switch (type)
	{
	case FoodType::NONE:
		break;
	case FoodType::Chicken:
		ChickenMove(obj, elapsedTime);
		break;
	case FoodType::Shrimp:
		ShrimpMove(obj, elapsedTime);
		break;
	default:
		break;
	}

	MainMove(obj, elapsedTime);

	static Key A('A');
	if (A.state(TRIGGER_MODE::RISINING_EDGE))
	{
		cook = !cook;
	}

	if (cook && !change)
	{
		Cookink(obj);
	}
}

void Food::Cookink(Obj3D* obj)
{
	change = true;

	switch (type) 
	{
	case FoodType::Chicken:
		obj->SetModelResource(pLoadModel.GetModelResource("Chicken"));
		break;
	case FoodType::Shrimp:
		obj->SetModelResource(pLoadModel.GetModelResource("Chicken"));
		break;
	}
}

void Food::ChickenMove(Obj3D* obj, float elapsedTime)
{
	VECTOR3 ang = obj->GetRotation();
	if (Escape(obj)) SetState(FoodState::Escape);
	else 
	{
		SetAcceleration(9.0f);
		SetDeceleration(6.0f);
		SetMaxSpeed(20.0f);
	}
	//if (Burst(obj)) SetState(FoodState::Burst);

	switch (state)
	{
	case Food::FoodState::Wait:
		waitTimer += elapsedTime;
		if (waitTimer >= GetFloatRandom(4.f, 10.f))
		{
			angle = GetFloatRandom(0.f, 259.f);
			obj->SetRotation(VECTOR3(ang.x, DirectX::XMConvertToRadians(angle), ang.z));
			float sn = ::sinf(angle);
			float cs = ::cosf(angle);
			moveDir = VECTOR3(sn, 0, cs);
			moveTimer = GetFloatRandom(2.f, 4.f);
			SetState(FoodState::NormalMove);
		}
		break;

	case Food::FoodState::NormalMove:
		moveTimer -= elapsedTime;
		AddForce(moveDir);

		if (moveTimer <= 0)
		{
			waitTimer = 0.f;
			SetState(FoodState::Wait);
		}
		break;

	case Food::FoodState::Escape:
		AddForce(-moveDir);

		if (handDist >= distance * 2)
		{
			waitTimer = 0.f;
			SetState(FoodState::Wait);
		}
		break;

	case Food::FoodState::Burst:

		break;

	default:
		break;
	}

	//#ifdef USE_IMGUI
	//	ImGui::Begin("Chikcen");
	//
	//	ImGui::Text("MoveState:%d", static_cast<int>(state));
	//	ImGui::Text("MoveTimer:%f", moveTimer);
	//	ImGui::Text("WaitTimer:%f", waitTimer);
	//
	//	ImGui::End();
	//#endif
}

void Food::ShrimpMove(Obj3D* obj, float elapsedTime)
{
}

bool Food::Escape(Obj3D* obj)
{
	VECTOR3 position = hand->GetPosition();
	VECTOR3 vec = position - obj->GetPosition();
	handDist = Vec3Length(vec);
	vec.y = 0.f;
	Vec3Normalize(&vec, vec);

	if (handDist < distance)
	{
		SetAcceleration(24.f);
		SetMaxSpeed(36.f);

		moveDir = vec;
		return true;
	}

	return false;
}

bool Food::Burst(Obj3D* obj)
{
	GetAlg<Player> get(hand->GetMoveAlg());
	get.Gettter();

	if (!burst)
	{
		SetAcceleration(100.f);
		SetMaxSpeed(200.f);

		angle = GetFloatRandom(0.f, 359.f);
		float sn = ::sinf(angle);
		float cs = ::cosf(angle);
		moveDir = VECTOR3(sn, 1, cs);
		Vec3Normalize(&moveDir, moveDir);
		AddForce(moveDir);
		return true;
	}

	return false;
}

void Food::AreaCheck(Obj3D* obj)
{

}


