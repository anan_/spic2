#pragma once

struct WaveData
{
	int number;
	char* modelTag;
	VECTOR3 scale;
	float angleY;
	VECTOR3 position;
};

