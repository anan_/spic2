
#include "sprite.h"
#include "create_DX11.h"
#include "window.h"

Sprite::Sprite(ID3D11Device* device, const wchar_t* filename)
{
	HRESULT hr = S_OK;

	Vertex vertices[] =
	{
		VECTOR3(-0.5f, 0.5f,0),VECTOR2(0,0), VECTOR4(1,1,1,1), //����
		VECTOR3(0.5f, 0.5f,0),VECTOR2(1,0), VECTOR4(1,1,1,1), //�E��
		VECTOR3(-0.5f,-0.5f,0),VECTOR2(0,1), VECTOR4(1,1,1,1), //����
		VECTOR3(0.5f,-0.5f,0),VECTOR2(1,1), VECTOR4(1,1,1,1), //�E��
	};

	{
		D3D11_BUFFER_DESC desc;
		ZeroMemory(&desc, sizeof(desc));
		desc.Usage = D3D11_USAGE_DYNAMIC;
		desc.ByteWidth = sizeof(Vertex) * 4;
		desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		D3D11_SUBRESOURCE_DATA data;
		ZeroMemory(&data, sizeof(data));
		data.pSysMem = vertices;

		hr = device->CreateBuffer(&desc, &data, vertexBuffer.GetAddressOf());
		if (FAILED(hr))
		{
			assert(!"CreateBuffer");
		}
	}

	{
		//D3D11_INPUT_ELEMENT_DESC inputElement[] =
		//{
		//	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		//	{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		//	{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		//};
		//UINT numElements = ARRAYSIZE(inputElement);

		//const char* vsName = "Data/shaders/cso/sprite_vs.cso";
		//const char* psName = "Data/shaders/cso/sprite_ps.cso";

		//ResourceManager::LoadVertexShaders(device, vsName, inputElement, numElements, VS.GetAddressOf(), layout.GetAddressOf());
		//ResourceManager::LoadPixelShaders(device, psName, PS.GetAddressOf());

		shader = std::make_unique<Shader>();
		shader->CreateSprite(device);
	}

	{
		//D3D11_RASTERIZER_DESC desc;
		//ZeroMemory(&desc, sizeof(desc));
		//desc.FillMode = D3D11_FILL_SOLID;
		//desc.CullMode = D3D11_CULL_NONE;
		//desc.FrontCounterClockwise = false;
		//desc.DepthBias = 0;
		//desc.DepthBiasClamp = 0.0f;
		//desc.SlopeScaledDepthBias = 0.0f;
		//desc.DepthClipEnable = true;
		//desc.ScissorEnable = false;
		//desc.MultisampleEnable = true;
		//desc.AntialiasedLineEnable = false;
		//hr = device->CreateRasterizerState(&desc, rasterizer.GetAddressOf());
		//if (FAILED(hr))
		//{
		//	assert(!"CreateRasterizerState");
		//}
	}

	{
		ResourceManager::LoadShaderResourceView(device, filename, SRV.GetAddressOf(), &texture2d);

		samplerState = std::make_unique<SamplerState>(device, 0);
		//D3D11_SAMPLER_DESC desc;
		//ZeroMemory(&desc, sizeof(desc));
		//desc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
		//desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		//desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		//desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		//desc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		//desc.MinLOD = -FLT_MAX;
		//desc.MaxLOD = FLT_MAX;
		//desc.MaxAnisotropy = 16;
		//desc.BorderColor[0] = 1.0f;
		//desc.BorderColor[1] = 1.0f;
		//desc.BorderColor[2] = 1.0f;
		//desc.BorderColor[3] = 1.0f;
		//hr = device->CreateSamplerState(&desc, samplerState.GetAddressOf());
		//if (FAILED(hr))
		//{
		//	assert(!"CreateSamplerState");
		//}
	}

	{
		//D3D11_DEPTH_STENCIL_DESC desc;
		//ZeroMemory(&desc, sizeof(desc));
		//desc.DepthEnable = false;
		//desc.StencilEnable = false;
		//desc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;
		//desc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;
		//desc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		//desc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
		//desc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_INCR;
		//desc.FrontFace.StencilFunc = D3D11_COMPARISON_GREATER_EQUAL;
		//desc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		//desc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
		//desc.BackFace.StencilPassOp = D3D11_STENCIL_OP_INCR;
		//desc.BackFace.StencilFunc = D3D11_COMPARISON_GREATER_EQUAL;
		//hr = device->CreateDepthStencilState(&desc, depthStencilState.GetAddressOf());
		//if (FAILED(hr))
		//{
		//	assert(!"CreateDepthStencilState");
		//}
	}
}

void Sprite::Render(ID3D11DeviceContext* context,
	const VECTOR2& pos, const VECTOR2& size,
	const VECTOR2& texPos, const VECTOR2& texSize,
	const float alpha)
{
	HRESULT hr = S_OK;

	Vertex vertices[] =
	{
		{ VECTOR3(pos.x, pos.y, 0), VECTOR2(texPos.x, texPos.y), VECTOR4(1,1,1,alpha) },
		{ VECTOR3(pos.x + size.x, pos.y, 0),VECTOR2(texPos.x + texSize.x, texPos.y), VECTOR4(1,1,1,alpha) },
		{ VECTOR3(pos.x, pos.y + size.y, 0),VECTOR2(texPos.x, texPos.y + texSize.y), VECTOR4(1,1,1,alpha) },
		{ VECTOR3(pos.x + size.x, pos.y + size.y, 0),VECTOR2(texPos.x + texSize.x, texPos.y + texSize.y), VECTOR4(1,1,1,alpha) },
	};

	for (int i = 0; i < 4; i++)
	{
		vertices[i].position.x -= pos.x + size.x / 2.0f;
		vertices[i].position.y -= pos.y + size.y / 2.0f;
	}

    float degree = DirectX::XMConvertToRadians(0.f);
	float sn = sinf(degree); float cs = cosf(degree);
	for (int i = 0; i < 4; i++)
	{
		float posX = vertices[i].position.x;
		float posY = vertices[i].position.y;
		vertices[i].position.x = posX * cs - posY * sn;
		vertices[i].position.y = posX * sn + posY * cs;
	}
	for (int i = 0; i < 4; i++)
	{
		vertices[i].position.x += pos.x + size.x / 2.0f;
		vertices[i].position.y += pos.y + size.y / 2.0f;
	}

	D3D11_MAPPED_SUBRESOURCE mapped;
	hr = context->Map(vertexBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped);
	if (FAILED(hr))
	{
		assert(!"Map");
	}
	Vertex* v = (Vertex*)mapped.pData;
	for (int i = 0; i < 4; i++)
	{
		v[i].position.x = (vertices[i].position.x / SCREEN_WIDTH) * 2.0f - 1.0f;
		v[i].position.y = -(vertices[i].position.y / SCREEN_HEIGHT) * 2.0f + 1.0f;
		v[i].color = vertices[i].color;
		v[i].texcoord.x = vertices[i].texcoord.x / texture2d.Width;
		v[i].texcoord.y = vertices[i].texcoord.y / texture2d.Height;
	}
	context->Unmap(vertexBuffer.Get(), 0);

	UINT stride = sizeof(Vertex);
	UINT offset = 0;
	context->IASetVertexBuffers(0, 1, vertexBuffer.GetAddressOf(), &stride, &offset);

	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	//context->RSSetState(rasterizer.Get());
	SetRender::SetRasterizerState(RS_CULL_NONE);
	SetRender::SetDepthStencilState(DS_FALSE);

	shader->Activate(context);

	context->PSSetShaderResources(0, 1, SRV.GetAddressOf());
	samplerState->Activate(context, 0);

	context->Draw(4, 0);
}

void Sprite::Render(ID3D11DeviceContext* context, 
	const VECTOR2& pos, const VECTOR2& size, 
	const VECTOR2& texPos, const VECTOR2& texSize, 
	const VECTOR2& center,
	const float alpha)
{
	HRESULT hr = S_OK;

	Vertex vertices[] =
	{
		{ VECTOR3(pos.x, pos.y, 0), VECTOR2(texPos.x, texPos.y), VECTOR4(1,1,1,alpha) },
		{ VECTOR3(pos.x + size.x, pos.y, 0),VECTOR2(texPos.x + texSize.x, texPos.y), VECTOR4(1,1,1,alpha) },
		{ VECTOR3(pos.x, pos.y + size.y, 0),VECTOR2(texPos.x, texPos.y + texSize.y), VECTOR4(1,1,1,alpha) },
		{ VECTOR3(pos.x + size.x, pos.y + size.y, 0),VECTOR2(texPos.x + texSize.x, texPos.y + texSize.y), VECTOR4(1,1,1,alpha) },
	};

	for (int i = 0; i < 4; i++)
	{
		vertices[i].position.x -= pos.x + size.x / 2.0f;
		vertices[i].position.y -= pos.y + size.y / 2.0f;
	}

	float degree = DirectX::XMConvertToRadians(0.f);
	float sn = sinf(degree); float cs = cosf(degree);
	for (int i = 0; i < 4; i++)
	{
		float posX = vertices[i].position.x;
		float posY = vertices[i].position.y;
		vertices[i].position.x = posX * cs - posY * sn;
		vertices[i].position.y = posX * sn + posY * cs;
	}
	for (int i = 0; i < 4; i++)
	{
		vertices[i].position.x += pos.x + size.x / 2.0f;
		vertices[i].position.y += pos.y + size.y / 2.0f;
	}

	D3D11_MAPPED_SUBRESOURCE mapped;
	hr = context->Map(vertexBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped);
	if (FAILED(hr))
	{
		assert(!"Map");
	}
	Vertex* v = (Vertex*)mapped.pData;
	for (int i = 0; i < 4; i++)
	{
		v[i].position.x = (vertices[i].position.x / SCREEN_WIDTH) * 2.0f - 1.0f;
		v[i].position.y = -(vertices[i].position.y / SCREEN_HEIGHT) * 2.0f + 1.0f;
		v[i].color = vertices[i].color;
		v[i].texcoord.x = vertices[i].texcoord.x / texture2d.Width;
		v[i].texcoord.y = vertices[i].texcoord.y / texture2d.Height;
	}
	context->Unmap(vertexBuffer.Get(), 0);

	UINT stride = sizeof(Vertex);
	UINT offset = 0;
	context->IASetVertexBuffers(0, 1, vertexBuffer.GetAddressOf(), &stride, &offset);

	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	//context->RSSetState(rasterizer.Get());
	SetRender::SetRasterizerState(RS_CULL_NONE);
	SetRender::SetDepthStencilState(DS_FALSE);

	shader->Activate(context);

	context->PSSetShaderResources(0, 1, SRV.GetAddressOf());
	samplerState->Activate(context, 0);

	context->Draw(4, 0);
}

void Sprite::Text(ID3D11DeviceContext* context, std::string str,
	float x, float y, float w, float h,
	float r, float g, float b, float a)
{
	float sw = static_cast<float>(texture2d.Width / 16);
	float sh = static_cast<float>(texture2d.Height / 16);
	float cursor = 0;
	for (char c : str)
	{
		LONG sx = c % 0x0F;

		Render(context,
			VECTOR2(x + cursor, y), VECTOR2(w, h),
			VECTOR2(sw*(c & 0x0F), sh*(c >> 4)), VECTOR2(sw, sh),
			0);

		cursor += w;
	}
}

void Sprite::Text(ID3D11DeviceContext* context, std::string str,
	const VECTOR2& position, const VECTOR2& size, 
	const VECTOR4& color)
{
	Text(context, str,
		position.x, position.y, size.x, size.y,
		color.x, color.y, color.z, color.w);
}

void SpriteBatch::Init(ID3D11Device* device, const wchar_t* filename, UINT instnce)
{
	HRESULT hr = S_OK;

	Vertex vertices[4] =
	{
		{ VECTOR3(0.0f, 0.0f, .0f), VECTOR2(0.0f, 0.0f) },
		{ VECTOR3(1.0f, 0.0f, .0f), VECTOR2(1.0f, 0.0f) },
		{ VECTOR3(0.0f, 1.0f, .0f), VECTOR2(0.0f, 1.0f) },
		{ VECTOR3(1.0f, 1.0f, .0f), VECTOR2(1.0f, 1.0f) },
	};

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_IMMUTABLE;
	bd.ByteWidth = sizeof(vertices);
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;
	bd.StructureByteStride = 0;
	D3D11_SUBRESOURCE_DATA data;
	ZeroMemory(&data, sizeof(data));
	data.pSysMem = vertices;
	data.SysMemPitch = 0;
	data.SysMemSlicePitch = 0;
	hr = device->CreateBuffer(&bd, &data, vertexBuffer.GetAddressOf());
	if (FAILED(hr))
	{
		assert(!"CreateBuffer");
	}

	maxInstance = instnce;
	instances = new Instance[maxInstance];

	ZeroMemory(&bd, sizeof(bd));
	bd.ByteWidth = sizeof(Instance) * maxInstance;
	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bd.MiscFlags = 0;
	bd.StructureByteStride = 0;
	ZeroMemory(&data, sizeof(data));
	data.pSysMem = instances;
	data.SysMemPitch = 0;
	data.SysMemSlicePitch = 0;
	hr = device->CreateBuffer(&bd, &data, instanceBuffer.GetAddressOf());
	if (FAILED(hr))
	{
		assert(!"CreateBuffer");
	}
	delete[] instances;

	//texture = std::make_unique<Texture>();
	//texture->Load(device, filename);

	ResourceManager::LoadShaderResourceView(device, filename, SRV.GetAddressOf(), &texture2d);

	samplerState = std::make_unique<SamplerState>(device, 0);
}

void SpriteBatch::Begin(ID3D11DeviceContext* context)
{
	HRESULT hr = S_OK;

	UINT stride[2] = { sizeof(Vertex), sizeof(Instance) };
	UINT offsets[2] = { 0, 0 };
	ID3D11Buffer* vbs[2] = { vertexBuffer.Get(), instanceBuffer.Get() };

	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	context->IASetVertexBuffers(0, 2, vbs, stride, offsets);
	SetRender::SetRasterizerState(RS_CULL_NONE);
	SetRender::SetDepthStencilState(DS_FALSE);

	shader->Activate(context);

	//texture->Set(context, 0);
	context->PSSetShaderResources(0, 1, SRV.GetAddressOf());
	samplerState->Activate(context, 0);

	D3D11_MAP map = D3D11_MAP_WRITE_DISCARD;
	D3D11_MAPPED_SUBRESOURCE mappedBuffer;
	hr = context->Map(instanceBuffer.Get(), 0, map, 0, &mappedBuffer);
	if (FAILED(hr))
	{
		assert(!"SpriteBatch Map");
	}
	instances = static_cast<Instance*>(mappedBuffer.pData);
	//memcpy(mappedBuffer.pData, instances, sizeof(Instance) * reserveNum);

	reserveNum = 0;
}

void SpriteBatch::RenderCenter(
	float dx, float dy, float dw, float dh,
	float sx, float sy, float sw, float sh,
	float cx, float cy,
	float angle,
	float r, float g, float b, float a,
	bool reverse)
{
	dx -= cx; dy -= cy; // �摜����

	float sn = sinf(angle * 0.01745f);
	float cs = cosf(angle * 0.01745f);
	float w = 2.f / SCREEN_WIDTH;
	float h = -2.f / SCREEN_HEIGHT;

	instances[reserveNum].ndcTransform._11 = w * dw * cs;
	instances[reserveNum].ndcTransform._21 = h * dw * sn;
	instances[reserveNum].ndcTransform._31 = 0.0f;
	instances[reserveNum].ndcTransform._41 = 0.0f;
	instances[reserveNum].ndcTransform._12 = w * dh * -sn;
	instances[reserveNum].ndcTransform._22 = h * dh * cs;
	instances[reserveNum].ndcTransform._32 = 0.0f;
	instances[reserveNum].ndcTransform._42 = 0.0f;
	instances[reserveNum].ndcTransform._13 = 0.0f;
	instances[reserveNum].ndcTransform._23 = 0.0f;
	instances[reserveNum].ndcTransform._33 = 1.0f;
	instances[reserveNum].ndcTransform._43 = 0.0f;
	instances[reserveNum].ndcTransform._14 = w * (-cx * cs + -cy * -sn + cx + dx) - 1.0f;
	instances[reserveNum].ndcTransform._24 = h * (-cx * sn + -cy * cs + cy + dy) + 1.0f;
	instances[reserveNum].ndcTransform._34 = 0.0f;
	instances[reserveNum].ndcTransform._44 = 1.0f;

	float tw = static_cast<float>(texture2d.Width);
	float th = static_cast<float>(texture2d.Height);
	if (reverse)
	{
		instances[reserveNum].texcoordTransform = VECTOR4(sx / tw + 1.f, sy / th, sw / tw, sh / th);
	}
	else
	{
		instances[reserveNum].texcoordTransform = VECTOR4(sx / tw, sy / th, sw / tw, sh / th);
	}
	instances[reserveNum].color = VECTOR4(r, g, b, a);

	reserveNum++;
}

void SpriteBatch::RenderCenter(const VECTOR2& position, const VECTOR2& size,
	const VECTOR2& texPosition, const VECTOR2& texSize,
	const VECTOR2& center, float angle,
	const VECTOR4& color, bool reverse)
{
	RenderCenter(position.x, position.y, size.x, size.y,
		texPosition.x, texPosition.y, texSize.x, texSize.y,
		center.x, center.y, angle,
		color.x, color.y, color.z, color.w,
		reverse);
}

void SpriteBatch::RenderLeft(float dx, float dy, float dw, float dh,
	float sx, float sy, float sw, float sh,
	float angle,
	float r, float g, float b, float a,
	bool reverse)
{
	float cx = dw * 0.5f, cy = dh * 0.5f;

	float sn = sinf(angle * 0.01745f);
	float cs = cosf(angle * 0.01745f);
	float w = 2.f / SCREEN_WIDTH;
	float h = -2.f / SCREEN_HEIGHT;

	instances[reserveNum].ndcTransform._11 = w * dw * cs;
	instances[reserveNum].ndcTransform._21 = h * dw * sn;
	instances[reserveNum].ndcTransform._31 = 0.0f;
	instances[reserveNum].ndcTransform._41 = 0.0f;
	instances[reserveNum].ndcTransform._12 = w * dh * -sn;
	instances[reserveNum].ndcTransform._22 = h * dh * cs;
	instances[reserveNum].ndcTransform._32 = 0.0f;
	instances[reserveNum].ndcTransform._42 = 0.0f;
	instances[reserveNum].ndcTransform._13 = 0.0f;
	instances[reserveNum].ndcTransform._23 = 0.0f;
	instances[reserveNum].ndcTransform._33 = 1.0f;
	instances[reserveNum].ndcTransform._43 = 0.0f;
	instances[reserveNum].ndcTransform._14 = w * (-cx * cs + -cy * -sn + cx + dx) - 1.0f;
	instances[reserveNum].ndcTransform._24 = h * (-cx * sn + -cy * cs + cy + dy) + 1.0f;
	instances[reserveNum].ndcTransform._34 = 0.0f;
	instances[reserveNum].ndcTransform._44 = 1.0f;

	float tw = static_cast<float>(texture2d.Width);
	float th = static_cast<float>(texture2d.Height);
	if (reverse)
	{
		instances[reserveNum].texcoordTransform = VECTOR4(sx / tw + 1.f, sy / th, sw / tw, sh / th);
	}
	else
	{
		instances[reserveNum].texcoordTransform = VECTOR4(sx / tw, sy / th, sw / tw, sh / th);
	}
	instances[reserveNum].color = VECTOR4(r, g, b, a);

	reserveNum++;
}

void SpriteBatch::RenderLeft(const VECTOR2& position, const VECTOR2& size,
	const VECTOR2& texPosition, const VECTOR2& texSize,
	float angle,
	const VECTOR4& color, bool reverse)
{
	RenderLeft(position.x, position.y, size.x, size.y,
		texPosition.x, texPosition.y, texSize.x, texSize.y,
		angle,
		color.x, color.y, color.z, color.w,
		reverse);
}

void SpriteBatch::End(ID3D11DeviceContext* context)
{
	context->Unmap(instanceBuffer.Get(), 0);

	context->DrawInstanced(4, reserveNum, 0, 0);
}

void SpriteBatch::Text(std::string str,
	float x, float y, float w, float h,
	float r, float g, float b, float a = 1.f)
{
	float sw = static_cast<float>(texture2d.Width / 16);
	float sh = static_cast<float>(texture2d.Height / 16);
	float cursor = 0;
	for (char c : str)
	{
		LONG sx = c % 0x0F;
		RenderLeft(x + cursor, y, w, h,
			sw * (c & 0x0F), sh * (c >> 4), sw, sh,
			0,
			r, g, b, a, false);

		cursor += w;
	}
}

void SpriteBatch::Text(std::string str,
	const VECTOR2& position, const VECTOR2& size,
	const VECTOR4& color)
{
	Text(str,
		position.x, position.y, size.x, size.y,
		color.x, color.y, color.z, color.w);
}

