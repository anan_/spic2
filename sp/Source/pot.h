#pragma once

#include "Actor.h"

class Pot : public Actor
{
public:
	void Move(Obj3D* obj, float elapsedTime);
};

class ErasePot : public EraseAlg
{
public:
	void Erase(Obj3D* obj, float elapsedTime)
	{
		obj->Release();
	}
};
