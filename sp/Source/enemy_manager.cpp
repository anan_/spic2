
#include "enemy_manager.h"

EnemyManager* EnemyManager::instance = nullptr;

void EnemyManager::Update(float elapsedTime)
{
    Obj3DManager::Update(elapsedTime);
}

void EnemyManager::Render(ID3D11DeviceContext* context, const FLOAT4X4& viewProjection)
{
    Obj3DManager::Render(context, viewProjection);
}
