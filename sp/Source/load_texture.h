#pragma once

#include "sprite.h"
#include <memory>
#include "vector.h"

enum class TextureTag
{
    MouseImage,

	PushStart,
    Retry,
    ReturnTitle,

	Max
};

struct TextureData
{
    TextureTag texTag;//テクスチャ番号
	const wchar_t* filename;//ファイル名
	UINT maxInstance; //最大数
};

class LoadTexture
{
	static LoadTexture* instance;
public:
    static const UINT textureMax = static_cast<UINT>(TextureTag::Max);
private:
	std::unique_ptr<SpriteBatch> sprite[textureMax];

public:
    LoadTexture(ID3D11Device* device);
    ~LoadTexture() {}

    void RenderLeft(ID3D11DeviceContext* context, int texNo, 
        const VECTOR2& position, const VECTOR2& size,
        const VECTOR2& texPos, const VECTOR2& texSize,
        float angle = 0.f,
        const VECTOR4& color = VECTOR4(1, 1, 1, 1),
        bool reverse = false);
    void RenderCenter(ID3D11DeviceContext* context, int texNo,
        const VECTOR2& position, const VECTOR2& size,
        const VECTOR2& texPos, const VECTOR2& texSize,
        const VECTOR2& center,
        float angle = 0.f,
        const VECTOR4& color = VECTOR4(1, 1, 1, 1),
        bool reverse = false);

public:
    static void Create(ID3D11Device* device)
    {
        if (instance != nullptr) return;
        instance = new LoadTexture(device);
    }
    static LoadTexture& GetInstance()
    {
        return *instance;
    }
    static void Destory()
    {
        if (instance != nullptr)
        {
            delete instance;
            instance = nullptr;
        }
    }
};