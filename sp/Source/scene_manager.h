#pragma once

#include "scene.h"
#include "create_DX11.h"

#include <map>
#include <string>
#include <memory>

class SceneManager
{
    static SceneManager* instance;
    Create_DX11* create_dx11;

    static std::map<std::string, std::unique_ptr<Scene>> scenes;
    const char* current_scene;

    SceneManager(HINSTANCE hInstance, Create_DX11* _create_dx11, ID3D11Device* device);
    ~SceneManager() { Release(); };

public:
    void Execute(HINSTANCE hInstance, ID3D11DeviceContext* context, ID3D11Device* device);
    void ChangeScene(ID3D11Device* device, const char* next_scene);
    void Release();

    void SetCurrentScene(const char* currentScene) { current_scene = currentScene; }
    const char* GetCurrentScene() const { return current_scene; }

    static void Create(HINSTANCE hInstance, Create_DX11* create_dx11, ID3D11Device* device)
    {
        if (instance != nullptr) return;

        instance = new SceneManager(hInstance, create_dx11, device);
    }
    static SceneManager& GetInstance()
    {
        return *instance;
    }
    static void Destory()
    {
        if (instance != nullptr)
        {
            delete instance;
            instance = nullptr;
        }
    }
};

#define ToSTRING(var) #var
enum { TITLE, GAME, CLEAR, OVER };