
#include "camera.h"
#include "create_DX11.h"
#include "window.h"
#include "input.h"
#include "mouse.h"
#include "add_obj.h"
#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

Camera* Camera::instance = nullptr;

// Translate 抜き取る
DirectX::XMMATRIX ExtractTranslate(const DirectX::XMMATRIX& world)
{
	return DirectX::XMMatrixTranslation(world.r[3].m128_f32[0], world.r[3].m128_f32[1], world.r[3].m128_f32[2]);
}
// scale 抜き取る
DirectX::XMMATRIX ExtractScaling(const DirectX::XMMATRIX& world) {
	return DirectX::XMMatrixScaling(
		DirectX::XMVector3Length(DirectX::XMVECTOR{ world.r[0].m128_f32[0],world.r[0].m128_f32[1],world.r[0].m128_f32[2] }).m128_f32[0],
		DirectX::XMVector3Length(DirectX::XMVECTOR{ world.r[1].m128_f32[0],world.r[1].m128_f32[1],world.r[1].m128_f32[2] }).m128_f32[0],
		DirectX::XMVector3Length(DirectX::XMVECTOR{ world.r[2].m128_f32[0],world.r[2].m128_f32[1],world.r[2].m128_f32[2] }).m128_f32[0]
		);
}
// rotate 抜き取る
DirectX::XMMATRIX ExtractRotation(const DirectX::XMMATRIX& world) {
	DirectX::XMMATRIX mOffset = ExtractTranslate(world);
	DirectX::XMMATRIX mScaling = ExtractScaling(world);

	DirectX::XMVECTOR det;
	// 左からScaling、右からOffsetの逆行列をそれぞれかける。
	return DirectX::XMMatrixInverse(&det, mScaling) * world * DirectX::XMMatrixInverse(&det, mOffset);
}

Camera::Camera()
{
	instance = this;

	distance = Vec3Length(focus - eye);

	// Projection
	SetProjection(30.f, 0.1f, 1000.f);
}

void Camera::SetProjection(const float viewAngle, const float nearZ, const float farZ)
{
	float aspect = SCREEN_WIDTH / SCREEN_HEIGHT;
	float fov = DirectX::XMConvertToRadians(viewAngle);
	SetPerspective(fov, aspect, nearZ, farZ);
}

void Camera::Update(float elapsedTime)
{
	static Key Space('C');
	if (Space.state(TRIGGER_MODE::RISINING_EDGE))
	{
		change = !change;	
		pAddObjManager.GetHnad()->SetPosition(VECTOR3(0,0,0));

	}
	//if (change)
	GameCamera(elapsedTime);
	//else
	//GameBackCamera(elapsedTime);

	//Cha(elapsedTime);

		//DebugCamera();
	CalculateView();

#ifdef USE_IMGUI
	ImGui::Begin("camera");

	ImGui::Text("Eye x:%.3f  y:%.3f  z %.3f", this->eye.x, this->eye.y, this->eye.z);
	ImGui::Text("Focus x:%.3f  y:%.3f  z %.3f", this->focus.x, this->focus.y, this->focus.z);

	ImGui::End();
#endif
}

void Camera::CalculateView()
{
	DirectX::XMVECTOR eye, focus, up;
	eye = DirectX::XMVectorSet(this->eye.x, this->eye.y, this->eye.z, 1.0f);
	focus = DirectX::XMVectorSet(this->focus.x, this->focus.y, this->focus.z, 1.0f);
	up = DirectX::XMVectorSet(this->up.x, this->up.y, this->up.z, 1.0f);

	DirectX::XMMATRIX V;
	V = DirectX::XMMatrixLookAtLH(eye, focus, up);
	DirectX::XMStoreFloat4x4(&view, V);

	this->right.x = view._11;
	this->right.y = view._12;
	this->right.z = view._13;

	this->up.x = view._21;
	this->up.y = view._22;
	this->up.z = view._23;

	this->front.x = view._31;
	this->front.y = view._32;
	this->front.z = view._33;
}

void Camera::GameCamera(float elapsedTime)
{
	Mouse& mouse = Mouse::GetInstance();

	static bool stop = false;
	static Key space(VK_SPACE);
	if (space.state(TRIGGER_MODE::RISINING_EDGE)) stop = !stop;

	const VECTOR2& move = mouse.GetMove();

	if (!stop)
	{
		rotateY += move.x * 0.2f;
		if (rotateY > DirectX::XM_PI)
		{
			rotateY -= DirectX::XM_2PI;
		}
		else if (rotateY < -DirectX::XM_PI)
		{
			rotateY += DirectX::XM_2PI;
		}

		if (move.x == 0.f)
		{
			mouse.SetScreenCenter(1920.f, 1080.f);
		}
	}
	const VECTOR3& playerPos = pAddObjManager.GetHnad()->GetPosition();
	const VECTOR3 pos(playerPos.x, playerPos.y + 30, playerPos.z);
	const VECTOR3& angle = pAddObjManager.GetHnad()->GetRotation();
	float sn = ::sinf(rotateY);
	float cs = ::cosf(rotateY);

	//float sx = ::sinf(ax);
	//float cy = ::cosf(ax);
	//float mouseX = ::sinf(angle.x);
	distance = 120.f;

	up = VECTOR3(0, 1.f, 0);
	eye = VECTOR3(pos.x - distance * sn, pos.y, pos.z - distance * cs);
	focus = playerPos;
}

void Camera::GameBackCamera(float elapsedTime)
{
	//this->SetFocus(VECTOR3(0.4f, 10.f, 100.f));

	//handの位置取得
	VECTOR3 pos = pAddObjManager.GetHnad()->GetPosition();
	//カメラのフォーカス
	VECTOR3 focus = this->GetFocus();
	//計算
	static float plus = 0;
	plus += elapsedTime;
	focus.x = pos.x;
	focus.y = pos.y;
	focus.z = -80.0f;
	this->SetFocus(focus);
	//
}

void Camera::Cha(float elapsedTime)
{

}

// 平行投影行列を算出する関数
FLOAT4X4 Camera::SetOrthographic(float w, float h, float znear, float zfar)
{
	DirectX::XMMATRIX P;
	P = DirectX::XMMatrixOrthographicLH(w, h, znear, zfar);
	DirectX::XMStoreFloat4x4(&projection, P);
	return projection;
}

// 透視投影行列を算出する関数
FLOAT4X4 Camera::SetPerspective(float fov, float aspect, float znear, float zfar)
{
	DirectX::XMMATRIX P;
	P = DirectX::XMMatrixPerspectiveFovLH(fov, aspect, znear, zfar);
	DirectX::XMStoreFloat4x4(&projection, P);
	return	projection;
}

const DirectX::XMMATRIX Camera::GetViewMatrix()
{
	return DirectX::XMLoadFloat4x4(&view);
}

const DirectX::XMMATRIX Camera::GetProjectionMatrix()
{
	return DirectX::XMLoadFloat4x4(&projection);
}

const FLOAT4X4 Camera::GetViewProection()
{
	DirectX::XMMATRIX C = DirectX::XMMatrixSet(
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
		);
	DirectX::XMMATRIX WVP;
	WVP = C * GetViewMatrix() * GetProjectionMatrix();
	DirectX::XMStoreFloat4x4(&viewProjection, WVP);
	return viewProjection;
}

void Camera::DebugCamera()
{
	POINT cursor;
	::GetCursorPos(&cursor);

	oldCursor = newCursor;
	newCursor = VECTOR2(static_cast<float>(cursor.x), static_cast<float>(cursor.y));

	float move_x = (newCursor.x - oldCursor.x) * 0.02f;
	float move_y = (newCursor.y - oldCursor.y) * 0.02f;

	static Key ALT(VK_MENU);
	static Key LBUTTON(VK_LBUTTON);
	static Key RBUTTON(VK_RBUTTON);
	static Key MBUTTON(VK_MBUTTON);
	if (ALT.state(TRIGGER_MODE::NONE))
	{
		if (LBUTTON.state(TRIGGER_MODE::NONE))
		{
			// Y軸回転
			rotateY += move_x * 0.2f;
			if (rotateY > DirectX::XM_PI)
			{
				rotateY -= DirectX::XM_2PI;
			}
			else if (rotateY < -DirectX::XM_PI)
			{
				rotateY += DirectX::XM_2PI;
			}
			// X軸回転
			rotateX += move_y * 0.2f;
			if (rotateX > DirectX::XM_PI)
			{
				rotateX += DirectX::XM_2PI;
			}
			else if (rotateX < -DirectX::XM_PI)
			{
				rotateX -= DirectX::XM_2PI;
			}
		}
		else if (MBUTTON.state(TRIGGER_MODE::NONE))
		{
			// 平行移動
			float s = distance * 0.035f;
			float x = -move_x * s;
			float y = move_y * s;

			focus.x += right.x * x;
			focus.y += right.y * x;
			focus.z += right.z * x;

			focus.x += up.x * y;
			focus.y += up.y * y;
			focus.z += up.z * y;
		}
		else if (RBUTTON.state(TRIGGER_MODE::NONE))
		{
			// ズーム
			distance += (-move_y - move_x) * distance * 0.1f;
		}
	}

	float sx = ::sinf(rotateX);
	float cx = ::cosf(rotateX);
	float sy = ::sinf(rotateY);
	float cy = ::cosf(rotateY);

	DirectX::XMVECTOR front = DirectX::XMVectorSet(-cx * sy, -sx, -cx * cy, 0.0f);
	DirectX::XMVECTOR right = DirectX::XMVectorSet(cy, 0, -sy, 0.0f);
	DirectX::XMVECTOR up = DirectX::XMVector3Cross(right, front);

	DirectX::XMVECTOR focus = DirectX::XMLoadFloat3(&this->focus);
	DirectX::XMVECTOR distance = DirectX::XMVectorSet(this->distance, this->distance, this->distance, 0.0f);
	DirectX::XMVECTOR eye = DirectX::XMVectorSubtract(focus, DirectX::XMVectorMultiply(front, distance));

	DirectX::XMMATRIX V = DirectX::XMMatrixLookAtLH(eye, focus, up);
	V = DirectX::XMMatrixTranspose(V);

	right = DirectX::XMVector3TransformNormal(DirectX::XMVectorSet(1, 0, 0, 0), V);
	up = DirectX::XMVector3TransformNormal(DirectX::XMVectorSet(0, 1, 0, 0), V);
	front = DirectX::XMVector3TransformNormal(DirectX::XMVectorSet(0, 0, 1, 0), V);

	// 方向ベクトル
	front = DirectX::XMVector3Normalize(front);
	DirectX::XMStoreFloat3(&this->front, front);

	DirectX::XMStoreFloat3(&this->eye, eye);
	DirectX::XMStoreFloat3(&this->up, up);
	DirectX::XMStoreFloat3(&this->right, right);
	this->right.y = 0.f;
}
