#pragma once

#include "actor.h"
#include "obj3d_manager.h"
#include "food.h"


enum class State
{
 
     Chicken ,
     Shrimp,
     Sushi,
     Order,
     Wait,//�ҋ@
};
class Guest : public Actor
{
public:
    void Move(Obj3D* obj, float elapsedTime);

    void SetWaitState();
    void UpdateWaitState(float elapsedTime);

    void SetSuhiState();
    void UpdateSushiState(float elapsedTime);


    void SetChickenState();
    void UpdateChickenState(float elapsedTime);

    void SetShrimpState();
    void UpdateShrimpState(float elapsedTime);

    void SetOrderState();
    void UpdateOrderState(float elapsedTime);


    const State& GetState() const { return state; }
    void SetState(int  o) { state = static_cast<State>(o); }
private:
    State state = State::Chicken;

    int  penalty = -5;

    float  timer  =  0;
    float  orderTime = 3;

    bool orderFlag = false;
    bool penaltyFlag = false;

    bool pflag = false;
};

class EraseGuest : public EraseAlg
{
public:
	void Erase(Obj3D* obj, float elapsedTime)
	{
		obj->Release();
	}
};


class GuestManager : public Obj3DManager
{
    static GuestManager* instance;
    FoodType order=FoodType::NONE;

public:
    GuestManager(ID3D11Device* device) : Obj3DManager(device) {}
    ~GuestManager() {}


    const FoodType& GetOrder() const { return order; }
    void SetOrder(int  o) { order = static_cast<FoodType>(o); }

    void Update(float elapsedTime) {

        Obj3DManager::Update(elapsedTime);
    }

public:
    static void Create(ID3D11Device* device)
    {
        if (instance != nullptr) return;
        instance = new GuestManager(device);
    }
    static GuestManager& GetInstance()
    {
        return *instance;
    }
    static void Destory()
    {
        if (instance != nullptr)
        {
            delete instance;
            instance = nullptr;
        }
    }
};

#define pGuestManager GuestManager::GetInstance()