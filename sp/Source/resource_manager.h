#pragma once

#include <d3d11.h>
#include <map>
#include <wrl.h>

#include "model_data.h"
#include "model.h"
#include "model_resource.h"

class ResourceManager
{
private:
    struct VertexShader_and_InputLayout
    {
        VertexShader_and_InputLayout(
            ID3D11VertexShader* vertexShader,
            ID3D11InputLayout* inputLayput)
            : vertexShader(vertexShader), inputLayout(inputLayput)
        {}
        Microsoft::WRL::ComPtr<ID3D11VertexShader> vertexShader;
        Microsoft::WRL::ComPtr<ID3D11InputLayout>  inputLayout;
    };

    static std::map <std::wstring, Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>> textures;
    static std::map <std::string, VertexShader_and_InputLayout>                      vertex_shader;
    static std::map <std::string, Microsoft::WRL::ComPtr<ID3D11PixelShader>>         pixel_shader;

	//struct ModelData_and_ModelResource
	//{
	//	ModelData_and_ModelResource(
	//		ModelData* data, ModelResource* resource)
	//		: data(data), resource(resource)
	//	{}
	//	std::unique_ptr<ModelData> data;
	//	std::shared_ptr<ModelResource> resource;

	//};
	//static std::map <std::string, ModelData_and_ModelResource> modelDatas;

    ResourceManager() {}
    ~ResourceManager() {}

public:

    static void Release()
    {
        textures.clear();
        vertex_shader.clear();
        pixel_shader.clear();
		//modelDatas.clear();
    }

    static bool LoadShaderResourceView(ID3D11Device* device, const wchar_t* filename,
        ID3D11ShaderResourceView** SRView, D3D11_TEXTURE2D_DESC* texDecs);
    static void ReleaseShaderResourceView(std::wstring wstr);

    static bool LoadVertexShaders(ID3D11Device* device, const char* filename,
        D3D11_INPUT_ELEMENT_DESC * elementDescs, int numElement,
        ID3D11VertexShader** vs, ID3D11InputLayout** il);
    static void ReleaseVertexShaders(std::string str);

    static bool LoadPixelShaders(ID3D11Device* device, const char* filename,
        ID3D11PixelShader** ps);
    static void ReleasePixelShaders(std::string str);
};
