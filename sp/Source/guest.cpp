
#include "guest.h"
#include "score.h"
#include "random.h"
#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

GuestManager* GuestManager::instance = nullptr;
void Guest::Move(Obj3D* obj, float elapsedTime)
{

	switch (state)
	{

	case State::Chicken:
		UpdateChickenState(elapsedTime);
		break;
	case State::Shrimp:
		UpdateShrimpState(elapsedTime);
		break;
	case State::Sushi:
		UpdateSushiState(elapsedTime);
		break;
	case State::Order:
		UpdateOrderState(elapsedTime);
		break;
	case State::Wait:
		UpdateWaitState(elapsedTime);
	}


	if (pGuestManager.GetOrder() != static_cast<FoodType>(-1))
	{
		orderFlag = true;
		if (pGuestManager.GetOrder() != static_cast<FoodType>(state))
		{
			if (state ==State::Sushi) penalty = -10;
			else penalty = -5;
			
			penaltyFlag = true;
		}
	}
	
	if (orderFlag)   SetOrderState(); 
	
	
	
#ifdef USE_IMGUI
	ImGui::Begin("Guest");
	ImGui::Text("state%d", state);
	ImGui::Text("time%f", timer);
	if (ImGui::Button("Set_Chiken"))
	{
		pGuestManager.SetOrder(1);

	}
	if (ImGui::Button("Set_Shrimp"))
	{
		pGuestManager.SetOrder(2);

	}
	ImGui::End();
#endif
}

void Guest::SetWaitState()
{
	state = State::Wait;
}


void Guest::UpdateWaitState(float elapsedTime)
{
	timer += elapsedTime;

	if (timer > 2)
	{
		SetChickenState();
	}
}

void Guest::SetSuhiState()
{
	state = State::Sushi;
}

void Guest::UpdateSushiState(float elapsedTime)
{
	timer += elapsedTime;

	if (timer > orderTime)
	{
		int a = GetIntRandom(0, 1);
		if (a == 0)
		{
			SetChickenState();
		}
		else
		{
			SetShrimpState();
		}
		timer = 0;
		pGuestManager.SetOrder(-1);
	}
}

void Guest::SetChickenState()
{
	state = State::Chicken;
}


void Guest::UpdateChickenState(float elapsedTime)
{
	timer += elapsedTime;

	if (timer > orderTime)
	{
		int a = GetIntRandom(0,100);
		if (a >35)
		{
			SetShrimpState();
		}
		else
		{
			SetSuhiState();
		}
		timer = 0;
		pGuestManager.SetOrder(-1);
	}
}

void Guest::SetShrimpState()
{
	state = State::Shrimp;
}

void Guest::UpdateShrimpState(float elapsedTime)
{
	timer += elapsedTime;

	if (timer > orderTime)
	{
		int a = GetIntRandom(0, 100);
		if (a > 35)
		{
			SetChickenState();
		}
		else
		{
			SetSuhiState();
		}
		
		timer = 0;
		pGuestManager.SetOrder(-1);
	}
}

void Guest::SetOrderState()
{
	orderFlag = false;
	pGuestManager.SetOrder(-1);
	state = State::Order; 
	pflag = penaltyFlag;
	penaltyFlag = false;
}

void Guest::UpdateOrderState(float elapsedTime)
{

	if (pflag)
	{
		Score::GetInstance().SetScore(Score::GetInstance().GetScore() -5);
	}
	else
	{
		Score::GetInstance().SetScore(Score::GetInstance().GetScore() + 10);

	}

	SetWaitState();
}


