
#include "scene_game.h"
#include "create_DX11.h"
using namespace GetCoreSystem;
#include "scene_manager.h"
#include "misc.h"
#include <string>
#include <sstream>
#include <iostream>
#include "obj3d.h"
#include "fadeout.h"
#include "font.h"
#include "random.h"
#include "load_model.h"
#include "collision.h"
#include "input.h"
#include "score.h"
#include "food_manager.h"

#include "add_obj.h"

#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

void SceneGame::Init(ID3D11Device* device)
{
	Camera& camera = Camera::GetInstance();
	camera.SetEye(VECTOR3(-0.2f, 20.f, -33.f));
	camera.SetFocus(VECTOR3(0.4f, 10.f, 1.f));

	LightInit(device);

	renderer = std::make_unique<ModelRenderer>(device);

	Score::GetInstance().Create(device);

	pAddObjManager.Create(device);

	pAddObjManager.AddField(device,
		"Field");

	pAddObjManager.AddKitchen(device,
		"Kitchen");

	pAddObjManager.AddPot(device,
		"Pot");

	pAddObjManager.AddHand(device,
		"Hand");

	pAddObjManager.AddGuest(device,
		"Pig");

	pAddObjManager.AddFood(device,
		"Bard", FoodType::Chicken);

	pFont.Create();
	pFont.Load(device);

	chickenImages = std::make_unique<Sprite>(device, L"Data/images/CHI.png");
	shrimpImages = std::make_unique<Sprite>(device, L"Data/images/エビフライ.png");
	sushiImages = std::make_unique<Sprite>(device, L"Data/images/sushi.png");
	tstpos = VECTOR2(0, 0);
	gametime = 0;
}

void SceneGame::Release()
{
	//pFoodManager.Destory();
	//pGuestManager.Destory();
	pAddObjManager.Destory();
	pFont.Destory();
}

void SceneGame::Update(ID3D11Device* device, float elapsedTime)
{
	
	Camera& camera = Camera::GetInstance();
	static float plus = 0;
	plus += elapsedTime;
	//VECTOR3 focus=camera.GetFocus();
	//focus.x = sinf(plus) * 7;
	//focus.y = 10.0f;
	//focus.z = 1.f;
	VECTOR3 handpos = pAddObjManager.GetHnad()->GetPosition();
	//camera.SetFocus(VECTOR3(pAddObjManager.GetHnad()->GetPosition().x, camera.GetFocus().y, pAddObjManager.GetHnad()->GetPosition().z));

	//camera.SetFocus(focus);
	camera.Update(elapsedTime);

	//static float plus = 0;
	//plus += elapsedTime * 3;
	//pFoodManager.GetList()->begin()->SetPosition(VECTOR3(plus, 0, plus));
	//pFoodManager.Update(elapsedTime);
	//pGuestManager.Update(elapsedTime);
	//potObj->Update(elapsedTime);
	pAddObjManager.Update(elapsedTime);
	Score::GetInstance().Update(elapsedTime);

	VECTOR3 outPosition, outNormal;
	static Key MouseLeft(VK_LBUTTON);
	// マウス座標取得
	POINT cursorPos;
	GetCursorPos(&cursorPos);
	ScreenToClient(GetHWND(), &cursorPos);

	VECTOR3 worldPositionNear, worldPositionFar;
	// スクリーン座標からワールド座標へ
	VECTOR3 screenPositionNear = {
	static_cast<float>(cursorPos.x),
	static_cast<float>(cursorPos.y),
	0.0f
	};
	VECTOR3 screenPositionFar = {
	static_cast<float>(cursorPos.x),
	static_cast<float>(cursorPos.y),
	1.0f
	};

	Collision::ScreenToWorld(&worldPositionNear, screenPositionNear);
	Collision::ScreenToWorld(&worldPositionFar, screenPositionFar);

	if (camera.GetMode() == true)
	{
		//pAddObjManager.GetHnad()->SetRotation(VECTOR3(0, DirectX::XMConvertToRadians(130), 0));

		//if (-1 != pAddObjManager.GetKitchen()->RayPick(worldPositionNear, worldPositionFar,
		//	&outPosition, &outNormal))
		//{
		//	pAddObjManager.GetHnad()->SetPosition(VECTOR3(outPosition.x, outPosition.y - 0.5, outPosition.z));
		//}
	}
	//カメラが後ろ向いてるとき
	if (camera.GetMode() == false)
	{
		pAddObjManager.GetHnad()->SetRotation(VECTOR3(0, DirectX::XMConvertToRadians(-40), 0));

		if (-1 != pAddObjManager.GetField()->RayPick(worldPositionNear, worldPositionFar,
			&outPosition, &outNormal))
		{
			pAddObjManager.GetHnad()->SetPosition(VECTOR3(outPosition.x, outPosition.y - 0.5, outPosition.z));
		}
		//if (handpos.x >= 80)
		//{
		//	pAddObjManager.GetHnad()->SetPosition(VECTOR3(80, handpos.y, handpos.z));
		//}
		//if (handpos.x <= -80)
		//{
		//	pAddObjManager.GetHnad()->SetPosition(VECTOR3(-80, handpos.y, handpos.z));
		//}

	}
	//if (-1 != pFoodManager.GetList()->begin()->RayPick(worldPositionNear, worldPositionFar,
	//	&outPosition, &outNormal))
	//{
	//	pAddObjManager.GetHnad()->SetPosition(outPosition);
	//}
	//左クリックが押されている時
	int i = 0;
	for (auto& food : *pFoodManager.GetList())
	{
		//くるくる
		food.SetRotation(VECTOR3(0, DirectX::XMConvertToRadians(180), cosf(plus)));

		if (MouseLeft.state(TRIGGER_MODE::RISINING_EDGE))
		{
			//鳥ちゃんクリックしたとき
			if (-1 != food.RayPick(worldPositionNear, worldPositionFar,
				&outPosition, &outNormal))
			{
				//適当な位置
				//food.SetPosition(potObj->GetPosition());

				int s = pAddObjManager.GetFood(i)->GetType();
				//pAddObjManager.GetFood(i)->burst = true;

				//pGuestManager.SetOrder(pAddObjManager.GetFood(i)->GetType());

				//GetAlg<Food> get(food.GetMoveAlg());

				//food.SetPosition(pAddObjManager.GetPot()->GetPosition());

				
			}
		}
		i++;
	}

	//arrow->Update(elapsedTime);
	//  
	//field->Update(elapsedTime);
	//kitchen->Update(elapsedTime);

	static Key Space(VK_SPACE);
	//if (Space.state(TRIGGER_MODE::RISINING_EDGE))
	//{
	//	pFadeOut.MoveStart();
	//}
	gametime += elapsedTime;
	if (gametime > 20)
	{
		pFadeOut.MoveStart("CLEAR");
	}
	if (pFadeOut.Update(elapsedTime))
	{
		SceneManager::GetInstance().ChangeScene(device, ToSTRING(CLEAR));
	}
#ifdef USE_IMGUI
	ImGui::Begin("focus");

	ImGui::Text("elapsedTime%5f", elapsedTime);
	ImGui::Text("elapsedTime%5f", elapsedTime);

	ImGui::End();

	ImGui::Begin("HandPos");

	ImGui::Text("X:%.2f", handpos.x);
	ImGui::Text("Y:%.2f", handpos.y);
	ImGui::Text("Z:%.2f", handpos.z);

	ImGui::End();

	ImGui::Begin("tstPos");

	ImGui::SliderFloat("tst_x", &tstpos.x, -100, 400);
	ImGui::SliderFloat("tst_y", &tstpos.y, -100, 400);

	ImGui::End();
#endif

}

void SceneGame::Render(ID3D11DeviceContext* context, float elapsedTime)
{
	Camera& camera = Camera::GetInstance();

	SetRender::SetBlender(BS_ALPHA);
	SetRender::SetDepthStencilState(DS_TRUE);
	SetRender::SetRasterizerState(RS_CULL_FRONT);

	LightUpdate(context);


	// モデルの描画
	{
		lightBuffer->Activate(context, 3);
		renderer->Begin(context, camera.GetViewProection());


		//renderer->Draw(context, field->GetModel());
		//renderer->Draw(context, kitchen->GetModel());
		//renderer->Draw(context, arrow->GetModel());
		//renderer->Draw(context, potObj->GetModel());

		//
		//float acs = 0;

		//for (auto& slime : *pEnemyManager.GetList())
		//{
		//	// カメラZが反転している

		//	DirectX::XMVECTOR start = DirectX::XMLoadFloat3(&camera.GetEye());
		//	DirectX::XMVECTOR end = DirectX::XMLoadFloat3(&slime.GetPosition());

		//	//DirectX::XMVECTOR l = DirectX::XMVector3TransformCoord(end, iw);
		//	DirectX::XMVECTOR vec = DirectX::XMVectorSubtract(end, start);
		//	DirectX::XMVECTOR len = DirectX::XMVector3Length(vec);
		//	float fLen;
		//	DirectX::XMStoreFloat(&fLen, len);


		//	DirectX::XMVECTOR cameraFront = DirectX::XMLoadFloat3(&camera.GetFront());

		//	DirectX::XMVECTOR cameraFront2 = DirectX::XMLoadFloat3(&(camera.GetFocus() - camera.GetEye()));
		//	cameraFront2.m128_f32[1] = vec.m128_f32[1] = 0;

		//	cameraFront = DirectX::XMVector3Normalize(cameraFront2);

		//	vec = DirectX::XMVector3Normalize(vec);


		//	DirectX::XMVECTOR dot = DirectX::XMVector3Dot(vec, cameraFront);


		//	float fdot;
		//	DirectX::XMStoreFloat(&fdot, dot);

		//	acs = acos(fdot);

		//	if (acs <= 15.f * 0.01745f)
		//	{
		//		renderer->Draw(context, slime.GetModel());
		//	}
		//}

		//renderer->Draw(context, playerBodyMove->GetCubeModel(), VECTOR4(1, 1, 1, 0.5f));

		renderer->Draw(context, pAddObjManager.GetField()->GetModel());
		renderer->Draw(context, pAddObjManager.GetHnad()->GetModel());
		renderer->Draw(context, pAddObjManager.GetKitchen()->GetModel());
		renderer->Draw(context, pAddObjManager.GetPot()->GetModel());
		for (auto& food : *pFoodManager.GetList())
		{
			if (renderer->CullingDot(food.GetPosition())) continue;

			renderer->Draw(context, food.GetModel());
		}

		//for (auto& guest : *pGuestManager.GetList())
		//{
		//	renderer->Draw(context, guest.GetModel());
		//	
		//}
		//if(pAddObjManager.GetGuest(0)->GetState() == State::Chicken)
		//{
		//	chickenImages->Render(context,tstpos,VECTOR2(400,200), VECTOR2(0, 0),VECTOR2(830,700),1);
		//}
		//if (pAddObjManager.GetGuest(0)->GetState() == State::Shrimp)
		//{
		//	shrimpImages->Render(context, tstpos, VECTOR2(400, 200), VECTOR2(0, 0), VECTOR2(1665, 953), 1);
		//}
		//if (pAddObjManager.GetGuest(0)->GetState() == State::Sushi)
		//{
		//	sushiImages->Render(context, tstpos, VECTOR2(225, 235), VECTOR2(0, 0), VECTOR2(262, 192), 1);
		//}

		

		renderer->End(context);
		lightBuffer->DeActivate(context);
	}

	pFont.Text(context, 1, VECTOR2(0, 0), VECTOR2(32, 32), VECTOR4(1, 1, 1, 1), "%d", Score::GetInstance().GetScore());

}

void SceneGame::LightInit(ID3D11Device* device)
{
	Light::Init(device);
	lightBuffer = std::make_unique<ConstantBuffer<CbLight>>(device);
}

void SceneGame::LightUpdate(ID3D11DeviceContext* context)
{
	Camera& camera = Camera::GetInstance();

	float XM_PI = 3.141592654f;
	static float lightAngle = XM_PI;
	Light::SetAmbient(VECTOR3(0.5f, 0.5f, 0.5f));
	//ライト方向
	LightDir.x = sinf(lightAngle);
	//でっけえライト
	LightDir.y = 0.0f;
	LightDir.z = cosf(lightAngle);
	static float angle = XM_PI / 4;
	//angle += elapsedTime;
	Light::SetDirLight(LightDir, VECTOR3(1.0f, 1.0f, 1.0f));

	lightBuffer->data.ambientColor = Light::Ambient;
	lightBuffer->data.lightDir = Light::LightDir;
	lightBuffer->data.lightColor = Light::DirLightColor;
	lightBuffer->data.eyePos.x = camera.GetEye().x;
	lightBuffer->data.eyePos.y = camera.GetEye().y;
	lightBuffer->data.eyePos.z = camera.GetEye().z;
	lightBuffer->data.eyePos.w = 1.0f;

	//memcpy(lightBuffer->data.PointLight, Light::PointLight, sizeof(POINTLIGHT)*Light::POINTMAX);
	//memcpy(lightBuffer->data.SpotLight, Light::SpotLight, sizeof(SPOTLIGHT)*Light::SPOTMAX);
}

void SceneGame::RenderShadow(ID3D11DeviceContext* context)
{
	//// レンダーターゲットを決める
	//ID3D11RenderTargetView* rtv = shadowMap->GetRenderTarget();
	//ID3D11DepthStencilView* dsv = GetDepthStencilView();
	//context->OMSetRenderTargets(1, &rtv, dsv);
	//// 画面とZステンシルバッファのクリア
	//float color[4] = { 1,1,1,1 };
	//context->ClearRenderTargetView(rtv, color);
	//context->ClearDepthStencilView(dsv, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	////デプスステンシルステート設定
	//context->OMSetDepthStencilState(GetDepthStencilState()->GetDepthStencilState(DS_TRUE), 1);
	////ブレンドステート設定
	//context->OMSetBlendState(GetBlendState()->GetBlendState(BLEND_LAVEL::BS_ALPHA), nullptr, 0xFFFFFFFF);
	////ライト位置にViewを設定
	//VECTOR3 t(0, 0, 0);
	//VECTOR3 p;
	//p.x = t.x - LightDir.x * 30.0f;
	//p.y = t.y - LightDir.y * 30.0f;
	//p.z = t.z - LightDir.z * 30.0f;

	//// ビューポートの設定
	//SetRender::SetViewPort(320, 120);
	//// ビューの設定
	//Camera v;
	//v.SetEye(p);
	//v.SetFocus(VECTOR3(0, 0, 0));
	//v.SetUp(VECTOR3(0, 1, 0));
	//// 平行投影
	//v.SetOrthographic(10.f, 10.0f, 1.0f, 100.0f);
	//v.Update(0.f);

	//{
	//	renderer->Begin(context, v.GetViewProection());
	//	for (auto& food : *pFoodManager.GetList())
	//	{
	//		renderer->Draw(context, food.GetModel());
	//	}
	//	renderer->Draw(context, pAddObjManager.GetField()->GetModel());
	//	renderer->End(context);
	//}

	//ID3D11RenderTargetView* backbuffer = GetRenderTargetView();
	//context->OMSetRenderTargets(1, &backbuffer, dsv);
}

/*
	// エディタ-
#ifdef USE_IMGUI


	ImGui::Begin("Editer");

	//ImGui::Text("Eye x:%.3f  y:%.3f  z %.3f", arrow->GetPosition().x, arrow->GetPosition().y, arrow->GetPosition().z);

	ImGuiTabBarFlags flag = ImGuiTabBarFlags_Reorderable; // タブをドラッグして並び替えできる
	flag |= ImGuiTabBarFlags_NoTabListPopupButton; // タブの一番左端にドロップダウンリストが表示される下向き三角形のクリックエリアを作成し、そこからタブを選択できるようになります。
	flag |= ImGuiTabBarFlags_AutoSelectNewTabs; // タブを新しく作成した時に自動でそのタブを選択状態にします。
												// flag |= ImGuiTabBarFlags_NoCloseWithMiddleMouseButton; // タブの中でマウス中央ボタンクリックすることでタブを閉じることができる機能を無効にします。
												// flag |= ImGuiTabBarFlags_NoTooltip; // タブ上にマウスオーバーした場合に表示されるタブ名のポップアップ表示を無効にします。
												// flag |= ImGuiTabBarFlags_FittingPolicyResizeDown; // タブがウィンドウ幅を超えてたくさんある場合にタブの幅を自動でリサイズしてフィットさせることができます。
	flag |= ImGuiTabBarFlags_FittingPolicyScroll;// タブの幅を自動でリサイズさせずに左右の矢印ボタンを右端に配置してそこからタブを順番に選択できるようにします。
	const char* names[5] = { "Player", "Camera", "Enemy", "Render", "Field" };// 同じタブ名を使いたい場合は、"##1" や "##2" のように区別させる必要があります
	static bool opend[5] = { true, true, true, true, true };

	static float fieldScale = 0;



	if (ImGui::BeginTabBar("TabBarID", flag))
	{
		for (int n = 0; n < IM_ARRAYSIZE(opend); n++)
		{
			// 第2引数の&opened[n]を省略すると閉じるボタン(X)が作成されません。
			if (opend[n] && ImGui::BeginTabItem(names[n]))
			{
				switch (n)
				{
				case 0: // Player

					break;

				case 1: // Camera
					ImGui::Text("Eye x:%.3f  y:%.3f  z %.3f", camera.GetEye().x, camera.GetEye().y, camera.GetEye().z);
					ImGui::Text("Focus x:%.3f  y:%.3f  z %.3f", camera.GetFocus().x, camera.GetFocus().y, camera.GetFocus().z);

					ImGui::Text("front x:%.3f  z %.3f", camera.GetFront().x, camera.GetFront().z);
					break;

				case 2: // Enemy
					if (ImGui::CollapsingHeader("EnemyAdd"))
					{
						//if (ImGui::TreeNode("Slime"))
						//{
						ImGui::BeginChild(ImGui::GetID((void*)0), ImVec2(250, 200), ImGuiWindowFlags_NoTitleBar);

						//static VECTOR3 pos;

						//ImGui::Text("EnemyNum:%02d", enemyNum);
						//ImGui::Text("Position");
						//ImGui::InputFloat("X", &pos.x, 0.01f, 1.0f, "%.1f");
						//ImGui::InputFloat("Y", &pos.y, 0.01f, 1.0f, "%.1f");
						//ImGui::InputFloat("Z", &pos.z, 0.01f, 1.0f, "%.1f");
						//if (ImGui::Button("Add") && enemyNum < SlimeMax)
						//{
						//	slimeMove[enemyNum] = std::make_shared<Slime>();
						//	slimeMove[enemyNum]->SetCamera(camera);
						//	pEnemyManager.Add(GetDevice(), pLoadModel.GetModelResource("Slime"),
						//		slimeMove[enemyNum].get(),
						//		VECTOR3(1, 1, 1), VECTOR3(0, 0, 0), pos);
						//	enemyNum++;
						//}
						ImGui::EndChild();

						//ImGui::TreePop();
						//}
					}

					//ImGui::Text("EnemyDead:%02d", deadCount);

					ImGui::BeginChild("ChildID", ImVec2(250, 260), true, flag);
					//for (auto& e : *pEnemyManager.GetList())
					//{

					//	ImGui::Text("X:%.2f", e.GetPosition().x);
					//	ImGui::Text("Y:%.2f", e.GetPosition().y);
					//	ImGui::Text("Z:%.2f", e.GetPosition().z);
					//}
					ImGui::EndChild();


					break;

				case 3: // Render
					break;

				case 4: // Field;

					ImGui::InputFloat("Scale", &fieldScale, 0.0f, 1.f, "%.1f");
					field->SetScale(VECTOR3(fieldScale, fieldScale, fieldScale));
					break;
				}

				// BeginTabItem() の終了処理
				ImGui::EndTabItem();
			}
		}

		// BeginTabBar() の終了処理
		ImGui::EndTabBar();
	}

	ImGui::End();
#endif
*/