#pragma once

#include "actor.h"
#include "slime.h"

class ErasePlayerShot;

class PlayerShot : public Actor
{
public:
    PlayerShot();
    ~PlayerShot() {}
    void Move(Obj3D* obj, float elapsedTime);

	//void SetEnemy(std::shared_ptr<Actor>& enemy) { slime = enemy; }
private:
	//std::shared_ptr<Slime> slime;
    std::unique_ptr<ErasePlayerShot> ezShot;
};

class ErasePlayerShot : public EraseAlg
{
public:
    void Erase(Obj3D* obj, float elapsedTime);
};
