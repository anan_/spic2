
#include "font.h"

Font* Font::instance = nullptr;

LoadFont fontTextures[FontLabel::FontMAX] =
{
	{ FontLabel::FONT0, L"Data/fonts/font0.png" },
	{ FontLabel::FONT1, L"Data/fonts/font1.png" },
	{ FontLabel::FONT2, L"Data/fonts/font2.png" },
	{ FontLabel::FONT3, L"Data/fonts/font3.png" },
	{ FontLabel::FONT4, L"Data/fonts/font4.png" },
	{ FontLabel::FONT5, L"Data/fonts/font5.png" },
	{ FontLabel::FONT6, L"Data/fonts/font6.png" },
};

void Font::Load(ID3D11Device* device)
{
	for (int i = 0; i < fontNum; i++)
	{
		font[i] = std::make_unique<SpriteBatch>(device, fontTextures[i].filename, maxInstance);
	}
}

void Font::Text(ID3D11DeviceContext* context, int fontNo,
	const VECTOR2& position, const VECTOR2& size,
	const VECTOR4& color,
	const char* str, ...)
{
	if (fontNo >= fontNum) return;

	char string[256];
	vsprintf_s(string, str, (char*)(&str + 1));
	std::string s(string);

	font[fontNo]->Begin(context);
	font[fontNo]->Text(s, position, size, color);
	font[fontNo]->End(context);
}
