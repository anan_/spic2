#pragma once
//**************************************
//   include Headers
//**************************************
#include "sound_buffer.h"


//**************************************
//   Object class
//**************************************
class Sound
{
	IXAudio2* m_p_xaudio2 = nullptr;
	IXAudio2MasteringVoice* m_p_mastering_voice = nullptr;

	std::unique_ptr<SoundBuffer> sound_buffer;

	const char* wav_name;
public:
	Sound(const char* name);
	~Sound();

	void Update();
	void Play( bool loop = false);
	bool Playing();
	void Pause();
	void Stop();
	void SetVolume(const float volume);
};