#include "cube_mesh.h"

CubeMesh::CubeMesh(ID3D11Device* device, const wchar_t* filename)
{
	HRESULT hr = S_OK;

	Vertex vertices[24] =
	{
		VECTOR3(0.5f,	0.5f,  -0.5f),	VECTOR3(0.0f, 0.0f, -1.0f), VECTOR2(1,0),VECTOR4(1,1,1,1),
		VECTOR3(-0.5f,  0.5f, -0.5f),	VECTOR3(0.0f, 0.0f, -1.0f), VECTOR2(0,0),VECTOR4(1,1,1,1),
		VECTOR3(0.5f,  -0.5f, -0.5f),	VECTOR3(0.0f, 0.0f, -1.0f), VECTOR2(1,1),VECTOR4(1,1,1,1),
		VECTOR3(-0.5f, -0.5f, -0.5f),	VECTOR3(0.0f, 0.0f, -1.0f), VECTOR2(0,1),VECTOR4(1,1,1,1),

		VECTOR3(-0.5f,	0.5f, 0.5f),	VECTOR3(0.0f, 0.0f, 1.0f),  VECTOR2(1,0),VECTOR4(1,1,1,1),
		VECTOR3(0.5f,  0.5f, 0.5f),		VECTOR3(0.0f, 0.0f, 1.0f),  VECTOR2(0,0),VECTOR4(1,1,1,1),
		VECTOR3(-0.5f,-0.5f, 0.5f),		VECTOR3(0.0f, 0.0f, 1.0f),  VECTOR2(1,1),VECTOR4(1,1,1,1),
		VECTOR3(0.5f, -0.5f, 0.5f),		VECTOR3(0.0f, 0.0f, 1.0f),  VECTOR2(0,1),VECTOR4(1,1,1,1),

		VECTOR3(0.5f,	0.5f, 0.5f),	VECTOR3(1.0f, 0.0f, 0.0f),  VECTOR2(1,0),VECTOR4(1,1,1,1),
		VECTOR3(0.5f,  0.5f, -0.5f),	VECTOR3(1.0f, 0.0f, 0.0f),  VECTOR2(0,0),VECTOR4(1,1,1,1),
		VECTOR3(0.5f, -0.5f, 0.5f),	    VECTOR3(1.0f, 0.0f, 0.0f),  VECTOR2(1,1),VECTOR4(1,1,1,1),
		VECTOR3(0.5f, -0.5f, -0.5f),	VECTOR3(1.0f, 0.0f, 0.0f),  VECTOR2(0,1),VECTOR4(1,1,1,1),

		VECTOR3(-0.5f,	0.5f, -0.5f),	VECTOR3(-1.0f, 0.0f, 0.0f), VECTOR2(1,0),VECTOR4(1,1,1,1),
		VECTOR3(-0.5f,  0.5f, 0.5f),	VECTOR3(-1.0f, 0.0f, 0.0f), VECTOR2(0,0),VECTOR4(1,1,1,1),
		VECTOR3(-0.5f, -0.5f, -0.5f),	VECTOR3(-1.0f, 0.0f, 0.0f), VECTOR2(1,1),VECTOR4(1,1,1,1),
		VECTOR3(-0.5f, -0.5f, 0.5f),	VECTOR3(-1.0f, 0.0f, 0.0f), VECTOR2(0,1),VECTOR4(1,1,1,1),

		VECTOR3(0.5f,	 0.5f,  0.5f),	VECTOR3(0.0f, 1.0f, 0.0f),  VECTOR2(1,0),VECTOR4(1,1,1,1),
		VECTOR3(-0.5f,  0.5f,  0.5f),	VECTOR3(0.0f, 1.0f, 0.0f),  VECTOR2(0,0),VECTOR4(1,1,1,1),
		VECTOR3(0.5f,  0.5f, -0.5f),	VECTOR3(0.0f, 1.0f, 0.0f),  VECTOR2(1,1),VECTOR4(1,1,1,1),
		VECTOR3(-0.5f,  0.5f, -0.5f),	VECTOR3(0.0f, 1.0f, 0.0f),  VECTOR2(0,1),VECTOR4(1,1,1,1),

		VECTOR3(0.5f,	 -0.5f, -0.5f),	VECTOR3(0.0f, -1.0f, 0.0f), VECTOR2(1,0),VECTOR4(1,1,1,1),
		VECTOR3(-0.5f,  -0.5f, -0.5f),	VECTOR3(0.0f, -1.0f, 0.0f), VECTOR2(0,0),VECTOR4(1,1,1,1),
		VECTOR3(0.5f,  -0.5f, 0.5f),	VECTOR3(0.0f, -1.0f, 0.0f), VECTOR2(1,1),VECTOR4(1,1,1,1),
		VECTOR3(-0.5f,  -0.5f, 0.5f),	VECTOR3(0.0f, -1.0f, 0.0f), VECTOR2(0,1),VECTOR4(1,1,1,1),

	};

	mesh.numVertices = sizeof(vertices) / sizeof(vertices[0]);
	D3D11_BUFFER_DESC desc;
	{
		ZeroMemory(&desc, sizeof(desc));
		desc.Usage = D3D11_USAGE_DEFAULT;
		desc.ByteWidth = sizeof(Vertex)*mesh.numVertices;
		desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		desc.CPUAccessFlags = 0;
		desc.MiscFlags = 0;
	}
	D3D11_SUBRESOURCE_DATA data;
	{
		ZeroMemory(&data, sizeof(data));
		data.pSysMem = vertices;
	}
	hr = device->CreateBuffer(&desc, &data, mesh.vertexBuffer.GetAddressOf());
	if (FAILED(hr))
	{
		assert(!"CreateBuffer");
	}

	unsigned int indices[36];
	for (int face = 0; face < 6; face++)
	{
		indices[face * 6] = face * 4;
		indices[face * 6 + 1] = face * 4 + 2;
		indices[face * 6 + 2] = face * 4 + 1;
		indices[face * 6 + 3] = face * 4 + 1;
		indices[face * 6 + 4] = face * 4 + 2;
		indices[face * 6 + 5] = face * 4 + 3;
	}		

	mesh.numIndices = sizeof(indices) / sizeof(indices[0]);
	{
		ZeroMemory(&desc, sizeof(desc));
		desc.Usage = D3D11_USAGE_DEFAULT;
		desc.ByteWidth = sizeof(Vertex)*mesh.numIndices;
		desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		desc.CPUAccessFlags = 0;
		desc.MiscFlags = 0;
	}
	{
		ZeroMemory(&data, sizeof(data));
		data.pSysMem = indices;
	}
	hr = device->CreateBuffer(&desc, &data, mesh.indexBuffer.GetAddressOf());
	if (FAILED(hr))
	{
		assert(!"CreateBuffer");
	}

	texture = std::make_unique<Texture>();
	if (filename)
	{
		texture->Load(device, filename);
	}
	else
	{
		texture->Dummy(device);
	}

	cbScene = std::make_unique<ConstantBuffer<CbScene>>(device);
	cbMesh = std::make_unique<ConstantBuffer<CbMesh>>(device);
	cbSubSet = std::make_unique<ConstantBuffer<CbSubset>>(device);

	shader = std::make_unique<Shader>();
	shader->CreateMesh(device);

	world = FLOAT4X4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
}

void CubeMesh::Update()
{
	DirectX::XMMATRIX S, R, T, W;
	S = DirectX::XMMatrixScaling(scale.x, scale.y, scale.z);
	R = DirectX::XMMatrixRotationRollPitchYaw(angle.x, angle.y, angle.z);
	T = DirectX::XMMatrixTranslation(position.x, position.y, position.z);
	W = S * R * T;
	DirectX::XMStoreFloat4x4(&world, W);
}

void CubeMesh::Render(ID3D11DeviceContext* context, const FLOAT4X4& viewProjection, const VECTOR4& color)
{
	shader->Activate(context);

	cbScene->data.view_projection = viewProjection;
	cbMesh->data.world = world;
	cbSubSet->data.materialColor = color;
	cbScene->Activate(context, 0);
	cbMesh->Activate(context, 1);
	cbSubSet->Activate(context, 2);

	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	UINT stride = sizeof(Vertex);
	UINT offset = 0;
	context->IASetVertexBuffers(0, 1, mesh.vertexBuffer.GetAddressOf(), &stride, &offset);
	context->IASetIndexBuffer(mesh.indexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);

	texture->Set(context, 1);

	context->DrawIndexed(mesh.numIndices, 0, 0);
}
