
#include "add_obj.h"
#include "load_model.h"

#include "random.h"
#include "collision.h"
#include "food_manager.h"

AddObj* AddObj::instance = nullptr;

AddObj::AddObj(ID3D11Device* device)
{
	pFoodManager.Create(device);
	pGuestManager.Create(device);
}

AddObj::~AddObj()
{
	pFoodManager.Destory();
	pGuestManager.Destory();
}

void AddObj::AddField(ID3D11Device* device,
	const char* modeTag)
{
	field = std::make_unique<Obj3D>();
	field->SetModelResource(pLoadModel.GetModelResource(modeTag));
	field->SetScale(VECTOR3(10, 10, 10));
	field->SetRotation(VECTOR3(0, 0, 0));
	field->SetPosition(VECTOR3(0, -50.f, 0));
	field->CalculateWorld();

	Collision::RegisterTerrain(field.get());
}

void AddObj::AddKitchen(ID3D11Device* device,
	const char* modeTag)
{
	kitchen = std::make_unique<Obj3D>();
	kitchen->SetModelResource(pLoadModel.GetModelResource(modeTag));
	kitchen->SetScale(VECTOR3(1, 1, 1));
	kitchen->SetRotation(VECTOR3(0, 0, 0));
	kitchen->SetPosition(VECTOR3(0, 0, 0));
	kitchen->CalculateWorld();
}

void AddObj::AddFood(ID3D11Device* device,
	const char* modeTag, FoodType type)
{
	for (int i = 0; i < BardMax; i++)
	{
		foods[i] = std::make_shared<Food>();
		foods[i]->SetType(type);
		foods[i]->SetHand(hand);

		//float rndX = GetFloatRandom();

		pFoodManager.Add(device, modeTag, foods[i],
			VECTOR3(1, 1, 1), VECTOR3(0, 0, 0), VECTOR3(0, 0, -50 + (-5 * i)));
	}
}

void AddObj::AddPot(ID3D11Device* device,
	const char* modeTag)
{
	pot = std::make_shared<Pot>();
	potObj = std::make_unique<Obj3D>();
	potObj->SetModelResource(pLoadModel.GetModelResource(modeTag));
	potObj->SetScale(VECTOR3(1.0f, 1.0f, 1.0f));
	potObj->SetRotation(VECTOR3(0, 0, 0));
	potObj->SetPosition(VECTOR3(-5, 13, -17));
	potObj->CalculateWorld();
}

void AddObj::AddHand(ID3D11Device* device,
	const char* modeTag)
{
	player = std::make_shared<Player>();
	hand = std::make_shared<Obj3D>();
	hand->SetModelResource(pLoadModel.GetModelResource(modeTag));
	hand->SetScale(VECTOR3(1.0f, 1.0f, 1.0f));
	hand->SetMoveAlg(player);
	hand->SetRotation(VECTOR3(0, 0, 0));
	hand->SetPosition(VECTOR3(0, 0, 0));
	hand->CalculateWorld();
}


void AddObj::AddGuest(ID3D11Device* device,
	const char* modeTag)
{
	guest = std::make_shared<Guest>();
	pGuestManager.Add(device, modeTag, guest,
		VECTOR3(1, 1, 1), VECTOR3(0, 0, 0), VECTOR3(2, 0, 5));

	//auto& it = *pGuestManager.GetList()->begin();
	//GetAlg<Guest> get(it.GetMoveAlg());
	//int test = get.Gettter()->Test();
	//if (test == 2)
	//{
	//	int r = test;
	//}
}

void AddObj::Update(float elapsedTime)
{
	pFoodManager.Update(elapsedTime);
	pGuestManager.Update(elapsedTime);
	if(potObj) potObj->Update(elapsedTime);
	if (hand) hand->Update(elapsedTime);
	if (field) field->Update(elapsedTime);
	if (kitchen) kitchen->Update(elapsedTime);

	RayPick();
}

void AddObj::RayPick()
{
	for (auto& food : *pFoodManager.GetList())
	{
		VECTOR3 pos = food.GetPosition();
		VECTOR3 start = VECTOR3(pos.x, pos.y + 10.f, pos.z);
		VECTOR3 end = VECTOR3(pos.x, pos.y - 10.f, pos.z);
		VECTOR3 outPosition, outNormal;
		if (-1 != Collision::RayPick(start, end, &outPosition, &outNormal))
		{
			food.SetPosition(outPosition);
		}
	}
}

