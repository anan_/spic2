#include "player.h"
#include "emulate_gamepad.h"
#include "camera.h"
Player::Player()
{
	SetAcceleration(9.0f);
	SetDeceleration(6.0f);
	SetMaxSpeed(50.0f);
}

void Player::Move(Obj3D* obj, float elapsedTime)
{
	// �ړ�
	Camera& camera = Camera::GetInstance();

	const VECTOR2& PAD = EmulateGamePad::WASD();
	const VECTOR3& right = camera.GetRight();
	const VECTOR3& front = camera.GetFront();
	VECTOR3 force;
	force.x = (right.x * PAD.x) + (front.x * PAD.y);
	force.y = 0;
	force.z = (-right.z * PAD.x) + (-front.z * PAD.y);

	AddForce(force);
	PlayerMove(obj, elapsedTime);

	//VECTOR3 outPos, position;
	//position = obj->GetPosition();
	//position.y += 2.f; force.y = position.y;
	//if (-1 != Collision::MoveCheck(position, force, &outPos))
	//{
	//	obj->SetPosition(outPos);
	//}

	//SetCube(VECTOR3(1, 1, 1), obj->GetRotation(), obj->GetPosition());
}

