#pragma once

#include <d3d11.h>
#include "vector.h"
#include "sampler_state.h"
#include "resource_manager.h"
#include "shader.h"
#include "texture.h"
#include <memory>

class Sprite
{
private:
	//        ComPtr<>
	ComPtr<ID3D11VertexShader> VS;
	ComPtr<ID3D11PixelShader> PS;
	ComPtr<ID3D11InputLayout> layout;

	std::unique_ptr<Shader> shader;

	ComPtr<ID3D11Buffer> vertexBuffer;

	ComPtr<ID3D11ShaderResourceView> SRV;
	D3D11_TEXTURE2D_DESC texture2d;
	std::unique_ptr<SamplerState> samplerState;

	ComPtr<ID3D11RasterizerState> rasterizer;
	ComPtr<ID3D11DepthStencilState> depthStencilState;

	struct Vertex
	{
		VECTOR3 position;
		VECTOR2 texcoord;
		VECTOR4 color;
	};

public:
	Sprite(ID3D11Device* device, const wchar_t* filename);
	virtual ~Sprite() { };

	void Render(ID3D11DeviceContext* context,
		const VECTOR2& pos, const VECTOR2& size,
		const VECTOR2& texPos, const VECTOR2& texSize,
		const float alpha);

	void Render(ID3D11DeviceContext* context,
		const VECTOR2& pos, const VECTOR2& size,
		const VECTOR2& texPos, const VECTOR2& texSize,
		const VECTOR2& center,
		const float alpha);

	void Text(ID3D11DeviceContext* context, std::string str,
		float x, float y, float w, float h,
		float r, float g, float b, float a);
	void Text(ID3D11DeviceContext* context, std::string str,
		const VECTOR2& position, const VECTOR2& size,
		const VECTOR4& color = VECTOR4(1, 1, 1, 1));

	const VECTOR2& GetTextureSize() const { return VECTOR2(static_cast<float>(texture2d.Width), static_cast<float>(texture2d.Height)); }
};

class SpriteBatch
{
protected:
	struct Vertex
	{
		VECTOR3 position;
		VECTOR2 texcoord;
	};

	struct Instance
	{
		FLOAT4X4 ndcTransform;
		VECTOR4 texcoordTransform;
		VECTOR4 color;
	};

	u_int maxInstance;

	std::unique_ptr<Shader> shader;
	ComPtr<ID3D11VertexShader> vertexShader = nullptr;
	ComPtr<ID3D11PixelShader> pixelShader = nullptr;
	ComPtr<ID3D11InputLayout> inputLayout = nullptr;

	ComPtr<ID3D11Buffer> vertexBuffer;

	std::unique_ptr<Texture> texture;

	ComPtr<ID3D11RasterizerState> rasterizer;

	ComPtr<ID3D11ShaderResourceView> SRV;
	D3D11_TEXTURE2D_DESC texture2d;
	std::unique_ptr<SamplerState> samplerState;

	ComPtr<ID3D11Buffer> instanceBuffer;
	Instance* instances;
	u_int reserveNum;

	void Init(ID3D11Device* device, const wchar_t* filename, UINT instnce);

public:
	SpriteBatch(ID3D11Device* device, const wchar_t* filename, UINT instnce)
	{
		shader = std::make_unique<Shader>();
		shader->CreateSpriteBatch(device);

		Init(device, filename, instnce);
	}
	~SpriteBatch() {}

	void Begin(ID3D11DeviceContext* context);
	void RenderCenter(float dx, float dy, float dw, float dh,
		float sx, float sy, float sw, float sh,
		float cx, float cy,
		float angle,
		float r, float g, float b, float a,
		bool reverse);
	void RenderCenter(const VECTOR2& position, const VECTOR2& size,
		const VECTOR2& texPosition, const VECTOR2& texSize,
		const VECTOR2& center, float angle,
		const VECTOR4& color,
		bool reverse);
	void RenderLeft(float dx, float dy, float dw, float dh,
		float sx, float sy, float sw, float sh,
		float angle,
		float r, float g, float b, float a,
		bool reverse);
	void RenderLeft(const VECTOR2& position, const VECTOR2& size,
		const VECTOR2& texPosition, const VECTOR2& texSize,
		float angle,
		const VECTOR4& color,
		bool reverse);
	void End(ID3D11DeviceContext* context);

	void Text(std::string str,
		float x, float y, float w, float h,
		float r, float g, float b, float a);
	void Text(std::string str,
		const VECTOR2& position, const VECTOR2& size,
		const VECTOR4& color);
};
