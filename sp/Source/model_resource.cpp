 
#include "WICTextureLoader.h"

#include "model_resource.h"
#include "model_data.h"
#include "misc.h"
#include "Shlwapi.h"
#include<fstream>
#include"fbx_loader.h"
#pragma comment( lib, "Shlwapi.lib" )



ModelData::ModelData(const char* filename, bool flipping_v_coodinates, std::string extension)
{
	std::string name = filename;
	int path = name.find_last_of(".") + 1;
	//d(drive)d(dir)f(fname)name
	std::string ddfname = name.substr(0, path);
	std::string path_name = ddfname + extension;
	if (PathFileExistsA(path_name.c_str()))
	{
		std::ifstream ifs;
		ifs.open(path_name, std::ios::binary);
		cereal::BinaryInputArchive i_archive(ifs);
		i_archive(*this);
	}
	else
	{

		FbxLoader fbx_loader;
		fbx_loader.Load(filename, *this);


		std::ofstream ofs;
		ofs.open(path_name, std::ios::binary);
		cereal::BinaryOutputArchive o_archive(ofs);
		o_archive(*this);
	}

}



ModelResource::ModelResource(ID3D11Device* device, std::unique_ptr<ModelData> _data)
{
	data = std::move(_data);

	// マテリアル
	materials.resize(data->materials.size());
	for (size_t materialIndex = 0; materialIndex < materials.size(); ++materialIndex)
	{
		auto&& src = data->materials.at(materialIndex);
		auto&& dst = materials.at(materialIndex);

		dst.color = src.color;

		// テクスチャ読み込み
		if (!src.textureFileName.empty())
		{
			errno_t err;
			const char* hullFilename = src.textureFileName.c_str();
			char filename[256], textureFile[256] = { 0 }, extension[256];

			// テクスチャ
			err = ::_splitpath_s(hullFilename, nullptr, 0, nullptr, 0, 
				filename, sizeof(filename) / sizeof(filename[0]), extension, sizeof(extension) / sizeof(extension[0]));
			if (err != 0)
			{
				assert(!"Not File");
			}
			sprintf_s(textureFile, "%s%s", filename, extension);

			// ディレクトリパス取得
			char dirName[256];
			::_splitpath_s(data->dirName, nullptr, 0, dirName, sizeof(dirName) / sizeof(dirName[0]), nullptr, 0, nullptr, 0);

			// 合成
			char FilePath[256];
			sprintf_s(FilePath, "%s%s", dirName, textureFile);

			// 変換
			size_t length;
			wchar_t wFilename[256];
			::mbstowcs_s(&length, wFilename, 256, FilePath, _TRUNCATE);

			Microsoft::WRL::ComPtr<ID3D11Resource> resource;
			HRESULT hr = DirectX::CreateWICTextureFromFile(device,
				wFilename, resource.GetAddressOf(), dst.shaderResourceView.GetAddressOf());
			if (FAILED(hr))
			{
				assert(!"CreateWICTextureFromFile");
			}
		}
	}

	// メッシュ
	meshes.resize(data->meshes.size());
	for (size_t meshIndex = 0; meshIndex < meshes.size(); ++meshIndex)
	{
		auto&& src = data->meshes.at(meshIndex);
		auto&& dst = meshes.at(meshIndex);

		// 頂点バッファ
		{
			D3D11_BUFFER_DESC desc = {};
			D3D11_SUBRESOURCE_DATA subData = {};

			desc.ByteWidth = static_cast<UINT>(sizeof(ModelData::Vertex) * src.vertices.size());
			//buffer_desc.Usage = D3D11_USAGE_DEFAULT;
			desc.Usage = D3D11_USAGE_IMMUTABLE;
			desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			desc.CPUAccessFlags = 0;
			desc.MiscFlags = 0;
			desc.StructureByteStride = 0;
			subData.pSysMem = src.vertices.data();
			subData.SysMemPitch = 0;
			subData.SysMemSlicePitch = 0;

			HRESULT hr = device->CreateBuffer(&desc, &subData, dst.vertexBuffer.GetAddressOf());
			_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
		}

		// インデックスバッファ
		{
			D3D11_BUFFER_DESC desc = {};
			D3D11_SUBRESOURCE_DATA subData = {};

			desc.ByteWidth = static_cast<UINT>(sizeof(u_int) * src.indices.size());
			//buffer_desc.Usage = D3D11_USAGE_DEFAULT;
			desc.Usage = D3D11_USAGE_IMMUTABLE;
			desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
			desc.CPUAccessFlags = 0;
			desc.MiscFlags = 0;
			desc.StructureByteStride = 0;
			subData.pSysMem = src.indices.data();
			subData.SysMemPitch = 0; //Not use for index buffers.
			subData.SysMemSlicePitch = 0; //Not use for index buffers.
			HRESULT hr = device->CreateBuffer(&desc, &subData, dst.indexBuffer.GetAddressOf());
			_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
		}

		dst.nodeIndex = src.nodeIndex;

		// サブセット
		dst.subsets.resize(src.subsets.size());
		for (size_t subsetIndex = 0; subsetIndex < src.subsets.size(); ++subsetIndex)
		{
			auto&& srcSubset = src.subsets.at(subsetIndex);
			auto&& dstSubset = dst.subsets.at(subsetIndex);

			dstSubset.startIndex = srcSubset.startIndex;
			dstSubset.indexCount = srcSubset.indexCount;
			dstSubset.material = &materials.at(srcSubset.materialIndex);
		}

		// ボーン変換行列用
		dst.nodeIndices.resize(src.nodeIndices.size());
		::memcpy(dst.nodeIndices.data(), src.nodeIndices.data(), sizeof(int) * dst.nodeIndices.size());

		dst.inverseTransforms.resize(src.inverseTransforms.size());
		for (size_t index = 0; index < dst.inverseTransforms.size(); ++index)
		{
			dst.inverseTransforms.at(index) = &src.inverseTransforms.at(index);
		}
	}
}
