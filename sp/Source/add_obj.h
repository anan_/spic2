#pragma once

#include <memory>
#include <array>

#include "food.h"
#include "pot.h"
#include "guest.h"
#include "player.h"
class AddObj
{
private:
    static AddObj* instance;

    std::shared_ptr<Obj3D> hand;
    std::unique_ptr<Obj3D> field;
    std::unique_ptr<Obj3D> kitchen;
    std::unique_ptr<Obj3D> potObj;

    static const int BardMax = 20;
    std::array< std::shared_ptr<Food>, BardMax> foods;
    std::shared_ptr<Pot> pot;
    std::shared_ptr<Guest> guest;
    std::shared_ptr<Player> player;

public:
    AddObj(ID3D11Device* device);
    ~AddObj();

    void AddField(ID3D11Device* device,
        const char* modeTag);

    void AddKitchen(ID3D11Device* device,
        const char* modeTag);

    void AddFood(ID3D11Device* device,
        const char* modeTag, FoodType type);

    void AddPot(ID3D11Device* device,
        const char* modeTag);

    void AddHand(ID3D11Device* device,
        const char* modeTag);

    void AddGuest(ID3D11Device* device,
        const char* modeTag);
    void Update(float elapsedTime);

    void RayPick();

     Obj3D* GetField() { return field.get(); }
     Obj3D* GetKitchen() { return kitchen.get(); }
     Obj3D* GetHnad() { return hand.get(); }
     Obj3D* GetPot() { return potObj.get(); }

     const std::shared_ptr<Food> GetFood(int num) const { return foods[num]; }
     const std::shared_ptr<Pot> GetPot() const { return pot; }

     const std::shared_ptr<Guest> GetGuest(int num) const { return guest; }
public:
    static void Create(ID3D11Device* device)
    {
        if (instance != nullptr) return;
        instance = new AddObj(device);
    }
    static AddObj& GetInstance()
    {
        return *instance;
    }
    static void Destory()
    {
        if (instance != nullptr)
        {
            delete instance;
            instance = nullptr;
        }
    }
};

#define pAddObjManager AddObj::GetInstance()
