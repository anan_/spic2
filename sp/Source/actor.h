#pragma once

#include "obj3d.h"
#include "camera.h"

struct ActorParam
{
	VECTOR3 velocity;
	float maxSpeed;
	float acceleration;
	float deceleration;
	float turnSpeed;
};

class Actor : public MoveAlg
{
public:
	Actor();
	void AddForce(const VECTOR3& force) { pram.velocity += force * pram.acceleration; }
	//void SetCamera(std::shared_ptr<Camera>& camera) { this->camera = camera; };

	const Obj3D* GetCube() const { return cube.get(); }
	Model* GetCubeModel() { return cube->GetModel(); }
	//bodyかheadか識別用　
	int parts;
	int Setparts(int parts) { return this->parts = parts; };
	void SetHand(std::shared_ptr<Obj3D>& hand) { this->hand = hand; }

protected:
	ActorParam pram;
	//std::shared_ptr<Camera> camera;
	std::shared_ptr<Obj3D> hand;
	std::unique_ptr<Obj3D> cube;

	VECTOR2 oldCursor, newCursor;

	void SetAcceleration(float acceleration) { pram.acceleration = acceleration; }
	void SetDeceleration(float deceleration) { pram.deceleration = deceleration; }
	void SetMaxSpeed(float maxSpeed) { pram.maxSpeed = maxSpeed; }
	void PlayerMove(Obj3D* obj, float elapsedTime);
	void SetCube(const VECTOR3& scale, const VECTOR3& rotation, const VECTOR3& position);

	void MainMove(Obj3D* obj, float elapsedTime);
};


