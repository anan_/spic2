#pragma once

#include <memory>
#include <d3d11.h>
#include "vector.h"

class Score
{
private:
    static Score* instance;
	
    int  score = 0;

public:
    static void Create(ID3D11Device* device)
    {
        if (instance != nullptr) return;
        instance = new Score(device);
    }
    static Score& GetInstance()
    {
        return *instance;
    }
    static void Destory()
    {
        if (instance != nullptr)
        {
            delete instance;
            instance = nullptr;
        }
    }

	Score(ID3D11Device* device);


	
    void Update(float elapsedTime);

	
    void SetScore(int s ) { score = s; }
    const int& GetScore()const { return score; }
};

