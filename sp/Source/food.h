#pragma once

#include "actor.h"

enum class FoodType
{
	NONE = -1,

	Chicken = 0, // ��
	Shrimp,      // �G�r

	MAX
};

class Food : public Actor
{
	FoodType type = FoodType::NONE;
	bool cook = false;
	bool change = false;

	enum class FoodState
	{
		Wait,
		NormalMove,
		Escape,
		Burst
	};
	FoodState state = FoodState::Wait;

	float waitTimer = 0.f;
	float moveTimer = 0.f;
	bool move = false;

	float angle = 0.f;
	VECTOR3 moveDir = {};

	const float distance = 10.f;
	float handDist = 0.f;
	VECTOR3 handPosition = {};


public:
	void Move(Obj3D* obj, float elapsedTime);

	void Cookink(Obj3D* obj);
	void SetType(FoodType type) 
	{
		this->type = type;

		SetAcceleration(9.0f);
		SetDeceleration(6.0f);
		SetMaxSpeed(20.0f);
	}
	void SetModelResource(Obj3D* obj, std::shared_ptr<ModelResource>& modelResource) { obj->SetModelResource(modelResource); }
	void SetState(FoodState state) { this->state = state; }

	// ��
	void ChickenMove(Obj3D* obj, float elapsedTime);
	// �G�r
	void ShrimpMove(Obj3D* obj, float elapsedTime);

	bool Escape(Obj3D* obj);
	bool burst = false;
	bool Burst(Obj3D* obj);

	void AreaCheck(Obj3D* obj);

	const int GetType() const { return static_cast<int>(type); }
};

class EraseFood : public EraseAlg
{
public:
	void Erase(Obj3D* obj, float elapsedTime)
	{
		obj->Release();
	}
};
