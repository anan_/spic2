#pragma once

#include "obj3d_manager.h"
#include "actor.h"

class Slime : public Actor
{
public:
    void Move(Obj3D* obj, float elapsedTime);
};

class EraseSlime : public EraseAlg
{
public:
	void Erase(Obj3D* obj, float elapsedTime)
	{
		obj->Release();
	}
};


