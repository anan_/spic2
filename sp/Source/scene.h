#pragma once

#include <d3d11.h>

class Scene
{
public:
	Scene() = default;
	virtual ~Scene() = default;

	virtual void Init(ID3D11Device* device) = 0;
	virtual void Release() = 0;
	virtual void Update(ID3D11Device* device, float elapsedTime) = 0;
	virtual void Render(ID3D11DeviceContext* context, float elapsedTime) = 0;
};

