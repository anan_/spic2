#pragma once

#include "obj3d_manager.h"

class ShotManager : public Obj3DManager
{
    static ShotManager* instance;

public:
    ShotManager(ID3D11Device* device) : Obj3DManager(device) {}
    ~ShotManager() {}

    void Update(float elapsedTime);
    void Render(ID3D11DeviceContext* context, const FLOAT4X4& viewProjection);

public:
    static void Create(ID3D11Device* device)
    {
        if (instance != nullptr) return;
        instance = new ShotManager(device);
    }
    static ShotManager& GetInstance()
    {
        return *instance;
    }
    static void Destory()
    {
        if (instance != nullptr)
        {
            delete instance;
            instance = nullptr;
        }
    }
};

#define pShotManager ShotManager::GetInstance()