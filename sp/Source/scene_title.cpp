
#include "scene_title.h"
#include "scene_manager.h"
#include "create_DX11.h"
using namespace GetCoreSystem;
#include "misc.h"
#include <string>
#include <sstream>
#include <iostream>
#include "input.h"
#include "mouse.h"
#include "load_model.h"

#include "fadeout.h"
#include "add_obj.h"

#include "food_manager.h"
#include "guest.h"
#include "collision.h"

#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

#define SPRITE_CENTER(texno,left,top,width,height)	{ (texno),(left),(top),(width),(height),(width)/2,(height)/2 }  // 画像の真ん中が中心

void SceneTitle::Init(ID3D11Device* device)
{
	LightInit(device);
	renderer = std::make_unique<ModelRenderer>(device);

	pAddObjManager.Create(device);

	pAddObjManager.AddField(device,
		"Cube2");

	pAddObjManager.AddKitchen(device,
		"Kitchen");

	pAddObjManager.AddPot(device,
		"Pot");

	pAddObjManager.AddHand(device,
		"Hand");

	pAddObjManager.AddGuest(device,
		"Pig");

	//startImg = std::make_unique<Sprite>(device, L"Data/images/gamestart.png");
	//as = std::make_unique<SpriteData>();

	startImg = std::make_unique<SpriteData>();
	startImg->SetTexture(TextureTag::PushStart, VECTOR2(0, 0), VECTOR2(690, 291));
}

void SceneTitle::Release()
{
	pAddObjManager.Destory();
}

void SceneTitle::Update(ID3D11Device* device, float elapsedTime)
{
	pAddObjManager.Update(elapsedTime);

	if (FadeOut::GetInstance().Update(elapsedTime))
	{
		SceneManager::GetInstance().ChangeScene(device, ToSTRING(GAME));
	}

	static Key Space(VK_SPACE);


	
}

void SceneTitle::Render(ID3D11DeviceContext* context, float elapsedTime)
{
	Camera& camera = Camera::GetInstance();

	SetRender::SetBlender(BS_ALPHA);
	SetRender::SetDepthStencilState(DS_TRUE);
	SetRender::SetRasterizerState(RS_CULL_FRONT);

	LightUpdate(context);

	lightBuffer->Activate(context, 3);
	renderer->Begin(context, camera.GetViewProection());
	// モデルの描画
	{
		renderer->Draw(context, pAddObjManager.GetField()->GetModel());
		renderer->Draw(context, pAddObjManager.GetHnad()->GetModel());
		renderer->Draw(context, pAddObjManager.GetKitchen()->GetModel());
		renderer->Draw(context, pAddObjManager.GetPot()->GetModel());
		for (auto& food : *pFoodManager.GetList())
		{
			renderer->Draw(context, food.GetModel());
		}

		for (auto& guest : *pGuestManager.GetList())
		{
			renderer->Draw(context, guest.GetModel());
		}
	}
	renderer->End(context);
	lightBuffer->DeActivate(context);

	// スプライト
	{
		VECTOR2 pos;
		pos = VECTOR2(560, 400);
		startImg->SetHitPosition(pos);
		startImg->SetHitScale(VECTOR2(0.5f, 0.5f));

		Mouse& mouse = Mouse::GetInstance();

		if (Collision::HitSpritePointer(startImg->GetRect(), mouse.GetScreenPosition()))
		{
			startImg->RendeCenter(pos, VECTOR2(0.3f, 0.3f));
			if (mouse.LeftButtonState(TRIGGER_MODE::RISINING_EDGE))
			{
				FadeOut::GetInstance().MoveStart("GAME");
			}
		}
		else
		{
			startImg->RendeCenter(pos, VECTOR2(0.5f, 0.5f));
		}

		//LoadTexture::GetInstance().RenderLeft(context, 0, VECTOR2(0,0), VECTOR2(690, 291), VECTOR2(0, 0), VECTOR2(690, 291), s);
		//LoadTexture::GetInstance().RenderCenter(context, 0, VECTOR2(345, 140), VECTOR2(690, 291), VECTOR2(0, 0), VECTOR2(690, 291), VECTOR2(345, 140), s);
	}

}

void SceneTitle::LightInit(ID3D11Device* device)
{
	Light::Init(device);
	lightBuffer = std::make_unique<ConstantBuffer<CbLight>>(device);
}

void SceneTitle::LightUpdate(ID3D11DeviceContext* context)
{
	Camera& camera = Camera::GetInstance();

	float XM_PI = 3.141592654f;
	static float lightAngle = XM_PI;
	Light::SetAmbient(VECTOR3(0.5f, 0.5f, 0.5f));
	//ライト方向
	LightDir.x = sinf(lightAngle);
	//でっけえライト
	LightDir.y = 0.0f;
	LightDir.z = cosf(lightAngle);
	static float angle = XM_PI / 4;
	//angle += elapsedTime;
	Light::SetDirLight(LightDir, VECTOR3(1.0f, 1.0f, 1.0f));

	lightBuffer->data.ambientColor = Light::Ambient;
	lightBuffer->data.lightDir = Light::LightDir;
	lightBuffer->data.lightColor = Light::DirLightColor;
	lightBuffer->data.eyePos.x = camera.GetEye().x;
	lightBuffer->data.eyePos.y = camera.GetEye().y;
	lightBuffer->data.eyePos.z = camera.GetEye().z;
	lightBuffer->data.eyePos.w = 1.0f;
}

