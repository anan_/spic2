
#pragma once

#include "scene.h"
#include <memory>
#include <array>
#include "font.h"

#include "vector.h"
#include "sprite.h"
#include "camera.h"
#include "constant_buffer.h"

#include "model.h"
#include "model_renderer.h"
#include "light.h"

#include "cube_mesh.h"
//#include "frame_buffer.h"

#include "obj3d.h"
#include "obj3d_manager.h"
#include "geometric_primitive.h"

#include "food.h"
#include "pot.h"
#include "guest.h"
class SceneGame : public Scene
{
private:
	VECTOR2 tstpos;
	std::unique_ptr<ModelRenderer> renderer;
	std::unique_ptr<Sprite> setumei_img;

	std::unique_ptr<Obj3D> arrow;
	std::unique_ptr<Obj3D> field;
	std::unique_ptr<Obj3D> kitchen;

	static const int BardMax = 5;
	std::array< std::unique_ptr<Food>, BardMax> bards;
	std::unique_ptr<Food> chicken;

	std::unique_ptr<Pot> pot;
	std::unique_ptr<Obj3D> potObj;

	std::unique_ptr<Guest> guest;

	std::unique_ptr<Sprite> chickenImages;
	std::unique_ptr<Sprite> shrimpImages;
	std::unique_ptr<Sprite> sushiImages;

	std::unique_ptr<Sprite> font;

	float time, currentSecond;
	int enemyNum, deadCount;
	bool clearFlg;

	float gametime = 0;
	struct Shadow
	{
		FLOAT4X4 world;
	};
	std::unique_ptr<ConstantBuffer<Shadow>> shadowBuffer;
	//std::unique_ptr<FrameBuffer> frameBuffer;

	struct CbLight
	{
		VECTOR4 lightColor;
		VECTOR4 lightDir;
		VECTOR4 ambientColor;
		VECTOR4 eyePos;

		POINTLIGHT PointLight[Light::POINTMAX];
		SPOTLIGHT SpotLight[Light::SPOTMAX];
	};
	std::unique_ptr<ConstantBuffer<CbLight>> lightBuffer;
	VECTOR3 light;
	// ���C�g
	VECTOR3 LightDir;

	void LightInit(ID3D11Device* device);
	void LightUpdate(ID3D11DeviceContext* context);
	void RenderShadow(ID3D11DeviceContext* context);

public:
	void Init(ID3D11Device* device);
	void Release();
	void Update(ID3D11Device* device, float elapsedTime);
	void Render(ID3D11DeviceContext* context, float elapsedTime);

};