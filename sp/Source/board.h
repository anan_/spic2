//#pragma once
//
//#include <d3d11.h>
//#include <wrl.h>
//#include <memory>
//#include "vector.h"
//#include "constant_buffer.h"
//#include "shader.h"
//
//
//using Microsoft::WRL::ComPtr;
//
//class Board
//{
//	VECTOR3 lt; 
//	VECTOR3 rt;
//	VECTOR3 lb;
//	VECTOR3 rb;
//	struct Vertex
//	{
//		VECTOR3 position;
//		VECTOR2 texcoord;
//		VECTOR4 color;
//	};
//
//	struct 
//		cbuffer
//	{
//		FLOAT4X4 worldViewProjection;
//		VECTOR4 color;
//	};
//
//	ComPtr<ID3D11Buffer> buffer;
//	ComPtr<ID3D11Buffer> constantBuffer;
//	std::unique_ptr<Shader> shader;
//	ComPtr<ID3D11RasterizerState> rasterizer;
//	ComPtr<ID3D11SamplerState> sampler;
//	ComPtr<ID3D11DepthStencilState> depthStensilState;
//	ComPtr<ID3D11ShaderResourceView> srv;
//	D3D11_TEXTURE2D_DESC texture2dDesc;
//
//public:
//	Board(ID3D11Device* device, const wchar_t* filename, bool oblon = true); // 長方形かどうか
//	void Render(ID3D11DeviceContext* context, const VECTOR3& position, const float& scale, 
//		const FLOAT4X4&  view, const FLOAT4X4& projection, const VECTOR4& color);
//};

#include<d3d11.h>
#include <DirectXMath.h>
#include "shader.h"
#include <wrl.h>
#include <memory>
class Board
{

	struct vertex
	{
		DirectX::XMFLOAT3 position;
		DirectX::XMFLOAT2 texcoord;
		DirectX::XMFLOAT4 color;
	};
	struct cbuffer
	{
		DirectX::XMFLOAT4X4 world_view_projection;
		DirectX::XMFLOAT4 material_color;
	};

	std::unique_ptr<Shader> shader;

	ID3D11VertexShader* vertexShader = nullptr;
	ID3D11PixelShader* pixelShader = nullptr;
	ID3D11Buffer* buffer = nullptr;
	ID3D11RasterizerState* rasterrizer = nullptr;
	ID3D11ShaderResourceView* srview = nullptr;
	D3D11_TEXTURE2D_DESC texture2d;
	ID3D11SamplerState* samplerstate = nullptr;
	ID3D11DepthStencilState* depthstencil = nullptr;
	ID3D11InputLayout* input = nullptr;
	ID3D11Buffer* constant_buffer=nullptr;

public:
	Board(ID3D11Device* device, const wchar_t* file_name);
	~Board()
	{
		vertexShader->Release();
		pixelShader->Release();
		buffer->Release();
		rasterrizer->Release();
		srview->Release();
		samplerstate->Release();
		depthstencil->Release();
		input->Release();
		constant_buffer->Release();
	}
	void render(ID3D11DeviceContext* immediate_context, const VECTOR3& position, const float& scale, 
		const FLOAT4X4& view, const FLOAT4X4& projection, const VECTOR4& color);
};
