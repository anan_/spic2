#pragma once

#include "model.h"

class Obj3D;

class MoveAlg
{
public:
    virtual void Move(Obj3D* obj, float elapsedTime) = 0;
};

template <class T>
struct GetAlg
{
	T* act;
	GetAlg(std::shared_ptr<MoveAlg>& mvAlg)
	{
		MoveAlg* temp = mvAlg.get();
		act = (T*)temp;
	}

	T* Gettter()
	{
		return act;
	}
};

class EraseAlg
{
public:
    virtual void Erase(Obj3D* obj, float elapsedTime) = 0;
};

class Obj3D
{
private:
	VECTOR3 scale = VECTOR3(1, 1, 1);
	VECTOR3 rotation = VECTOR3(0, 0, 0);
	VECTOR3 position = VECTOR3(0, 0, 0);
	FLOAT4X4 world = FLOAT4X4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);

	bool exist = false;
	VECTOR3 prevPosition;

	std::shared_ptr<Model> model = nullptr;
	std::shared_ptr<MoveAlg> mvAlg = nullptr;
	EraseAlg* ezAlg = nullptr;

public:
	Obj3D() {}
	virtual ~Obj3D() {}

	void Update(float elapsedTime);
	void CalculateWorld();
	int RayPick(const VECTOR3& startPosition, const VECTOR3& endPosition,
		VECTOR3* outPosition, VECTOR3* outNormal);
	void Release();

	const VECTOR3 GetScale() const { return scale; }
	const VECTOR3 GetRotation() const { return rotation; }
	const VECTOR3 GetPosition() const { return position; }
	Model* GetModel() { return model.get(); }
	const bool GetExist() const { return exist; }
	const VECTOR3 GetPrevPosition() const { return prevPosition; }
    std::shared_ptr<MoveAlg> GetMoveAlg() { return mvAlg; }

	void SetScale(const VECTOR3& scale) { this->scale = scale; };
	void SetRotation(const VECTOR3& rotation) { this->rotation = rotation; }
	void SetPosition(const VECTOR3& position) { this->position = position; }
	void SetPositionX(float x) { position.x = x; }
	void SetPositionY(float y) { position.y = y; }
	void SetPositionZ(float z) { position.z = z; }
	void SetExist(bool exist) { this->exist = exist; }
	void SetModelResource(std::shared_ptr<ModelResource>& modelResource) { model = std::make_shared<Model>(modelResource); }
	void SetModel(std::shared_ptr<Model>& model) { this->model = model; }
	void SetMoveAlg(std::shared_ptr<MoveAlg> mvAlg) { this->mvAlg = mvAlg; }
	void SetEraseAlg(EraseAlg* ezAlg) { this->ezAlg = ezAlg; }
};