#pragma once

#include "sprite.h"
#include <memory>

class FadeOut
{
private:
    static FadeOut* instance;

	std::unique_ptr<Sprite> fade;
	bool moveFlag;
	float colorW;
    const char* nextScene = nullptr;

	FadeOut(ID3D11Device* device)
	{
		moveFlag = false;
		colorW = 0.f;

        fade = std::make_unique<Sprite>(device, L"Data/images/siro.png");
	}

public:
    FadeOut() {}
    ~FadeOut() {}

	bool Update(float elapsedTime);
    void ChangeScene(ID3D11Device* device, float elapsedTime);
	void MoveStart(const char* nextScene);
	void Render(ID3D11DeviceContext* context);

    void SetScene(const char* nextScene) { this->nextScene = nextScene; }

    static void Create(ID3D11Device* device)
    {
        if (instance != nullptr) return;
        instance = new FadeOut(device);
    }
    static FadeOut& GetInstance()
    {
        return *instance;
    }
    static void Destory()
    {
        if (instance != nullptr)
        {
            delete instance;
            instance = nullptr;
        }
    }
};

#define pFadeOut FadeOut::GetInstance()
