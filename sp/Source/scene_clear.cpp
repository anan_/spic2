
#include "scene_clear.h"
#include "create_DX11.h"
using namespace GetCoreSystem;
#include "input.h"
#include "mouse.h"
#include "font.h"
#include "fadeout.h"
#include "collision.h"

#include "score.h"

void SceneClear::Init(ID3D11Device* device)
{
	retryImg = std::make_unique<SpriteData>();
	retryImg->SetTexture(TextureTag::Retry, VECTOR2(0, 0), VECTOR2(576, 128));
	returnTitleImg = std::make_unique<SpriteData>();
	returnTitleImg->SetTexture(TextureTag::ReturnTitle, VECTOR2(0, 0), VECTOR2(576, 128));

	Font::Create();
	Font::GetInstance().Load(device);
}

void SceneClear::Release()
{
	Score::GetInstance().Destory();
	Font::Destory();
}

void SceneClear::Update(ID3D11Device* device,float elapsedTime)
{
}

void SceneClear::Render(ID3D11DeviceContext* context, float elapsedTime)
{
	SetRender::SetBlender(BS_ALPHA);
	SetRender::SetDepthStencilState(DS_TRUE);
	SetRender::SetRasterizerState(RS_CULL_FRONT);

	// スプライト
	{
		Mouse& mouse = Mouse::GetInstance();

		VECTOR2 pos1, pos2;
		pos1 = VECTOR2(450, 300);
		retryImg->SetHitRect(pos1, VECTOR2(0.5f, 0.5f));
		pos2 = VECTOR2(450, 490);
		returnTitleImg->SetHitRect(pos2, VECTOR2(0.5f, 0.5f));

		if (Collision::HitSpritePointer(retryImg->GetRect(), mouse.GetScreenPosition()))
		{
			retryImg->RendeCenter(pos1, VECTOR2(0.3f, 0.3f));
			if(mouse.LeftButtonState(TRIGGER_MODE::NONE))
			{ 
				FadeOut::GetInstance().MoveStart("GAME");
			}
		}
		else
		{
			retryImg->RendeCenter(pos1, VECTOR2(0.5f, 0.5f));
		}
		if (Collision::HitSpritePointer(returnTitleImg->GetRect(), mouse.GetScreenPosition()))
		{
			returnTitleImg->RendeCenter(pos2, VECTOR2(0.3f, 0.3f));
			if (mouse.LeftButtonState(TRIGGER_MODE::NONE))
			{
				FadeOut::GetInstance().MoveStart("TITLE");
			}
		}
		else
		{
			returnTitleImg->RendeCenter(pos2, VECTOR2(0.5f, 0.5f));
		}

		Font::GetInstance().Text(context, 1, VECTOR2(120, 120), VECTOR2(120, 120), VECTOR4(1, 1, 1, 1), "%d", Score::GetInstance().GetScore());
	}
}
